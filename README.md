# Orionology

![Orionology](orionology_icon.png)

Application reading Synapse Audio SFS (Sonic Factory Song) song file format. Application prints out some of the song data in somewhat geeky manner, but allows inspection of the song files. In the current state it does not work for broken files nor makes attempt to fix such.

Same data is used for Orion Pro and Orion Platinum files. Some data is just defaulted with Orion Pro when saving. Technically afair Orion Pro could load Platinum version files and play them, with difference that all data was not editable and some effects/generators might have not been available.

Orion Pro was later discontinued and Orion Platinum was rebranded from version 8 as Orion. 

This development would not have been possible without access to Orion's C++ source code and permission by Richard Hoffmann from Synapse Audio. I (Jouni Airaksinen) worked as freelancer programmer (somewhere between 2000 - 2010 or so) for Synapse Audio developing the Orion, so I have/had access and inside knownledge on the Orion source code.

#### Author

Jouni Airaksinen
jouni@kettureaktio.fi

## Usage

This is command line tool. After either downloading built apps or building your own, you run it with:

`orionlogy songfile.sfs`

### Developers

Developers can use `cargo run` when building from source and to change logging level `RUST_LOG=debug cargo run`. 

## Development

### Setting up on macOS

See: https://www.rust-lang.org/learn/get-started

### Setting up on Windows

See: https://www.rust-lang.org/learn/get-started

### Setting up on Linux

See: https://www.rust-lang.org/learn/get-started

### C header bindings

App uses copied header info from Orion. If you modify those, you need to run `./gen-bindings.sh` to update the `bindings.rs`.

## Notes about code

Code is fairly functional style and does not take use of Rust struct impl functions very well right now. This is the alpha version which definitely could be improved. While all refactoring could be done, it mostly boils now down to the fact that app works and as Orion is discontinued product, there is very little use of improving something which works and won't probably be used that much. Application is more of a solution to certain short term need and explorational sample to realm of Orion song files.

That said, here are places which could improve code quality:

- Wrap different chunks and loaders into own structs with specific readers
- Split code into multiple files instead of one large
- Take note on using usize vs u32 vs u64, now those are used inconsitent way
- Error handling
- Readers should read data field by field instead of using generated struct bindings and unsafe data reading
- Chunk scanning for broken file tolerance
- String used where &str probably would be enough (mostly due the c-string reading)
- Strings assumed to be UTF-8, while it's most likely not (probably CP-1252 as Orion is Windows application)
- TESTS, maybe have the TestSong.sfs in different breakpoint versions and automatic test run for those

Possible new features:

- add writer support (for modifying files)

## Supported Modules (Chunks)

Reference source code used was Orion 8.000 (beta) `0x8000`. Any changes to reader code since that may (skipped) or may not be processed properly.

### Generators

- [x] SF_OBJ_SAMPLER
- [x] SF_OBJ_WASP
- [x] SF_OBJ_909
- [x] SF_OBJ_VST2SYNTH
- [x] SF_OBJ_303
- [x] SF_OBJ_WAVE
- [x] SF_OBJ_AUDIO
- [x] SF_OBJ_DRUMS
- [x] SF_OBJ_MIDI
- [x] SF_OBJ_TOMKICK
- [x] SF_OBJ_HIHATS  (not used in Orion)
- [x] SF_OBJ_SCREAMER
- [x] SF_OBJ_PLUCKED
- [x] SF_OBJ_AUTOMATION  (not used in Orion?)
- [x] SF_OBJ_ULTRAN
- [x] SF_OBJ_REWIRE
- [x] SF_OBJ_FMSYNTH (not used in Orion)
- [x] SF_OBJ_GRAND (Orion 8)
- [x] SF_OBJ_DXSYNTH
- [ ] SF_OBJ_P8	(not used in Orion)
- [x] SF_OBJ_P9 (Orion 8)

### Effects

Missing may work, unless they have special non-chunked data.

- [x] SF_OBJ_DELAY
- [x] SF_OBJ_REVERB
- [x] SF_OBJ_PHASER
- [x] SF_OBJ_DISTORTION
- [x] SF_OBJ_VST
- [x] SF_OBJ_COMPRESSOR
- [x] SF_OBJ_TRANCEGATE
- [x] SF_OBJ_DELAY_STEREO
- [x] SF_OBJ_CHORUS
- [x] SF_OBJ_PCF
- [x] SF_OBJ_ULTRAVERB
- [x] SF_OBJ_X_DELAY
- [x] SF_OBJ_TS9DIST
- [x] SF_OBJ_DX
- [x] SF_OBJ_FILTER
- [x] SF_OBJ_NUPHASER
- [ ] SF_OBJ_FLANGER (not used in Orion)
- [x] SF_OBJ_FILTERDELAY
- [x] SF_OBJ_ECHO
- [x] SF_OBJ_ULTRAVERB2
- [x] SF_OBJ_PLATE_REVERB
- [x] SF_OBJ_RINGMOD
- [x] SF_OBJ_ANALYZER (not used in Orion)
- [ ] SF_OBJ_TALKBOX (not used in Orion)
- [x] SF_OBJ_GATE
- [x] SF_OBJ_CLIPPER
- [ ] SF_OBJ_VOCODER (not used in Orion)
- [x] SF_OBJ_PARAM_EQ
- [x] SF_OBJ_LIMITER
- [x] SF_OBJ_LATENCY
- [x] SF_OBJ_CHORUS2
- [x] SF_OBJ_STEREOWIDEN
- [x] SF_OBJ_PLAT_ECHO
- [x] SF_OBJ_TREMOLO
- [x] SF_OBJ_LESLIE
- [ ] SF_OBJ_VOLUME (not used in Orion)
- [x] SF_OBJ_HARM_FILTER
- [x] SF_OBJ_FILTER_24
- [x] SF_OBJ_BAND_SPLIT
- [ ] SF_OBJ_HARM_DELAY (not used in Orion)
- [x] SF_OBJ_REVERB3
- [x] SF_OBJ_FXCONTAINER
- [x] SF_OBJ_LOFI
- [x] SF_OBJ_COMP_MULTI
- [x] SF_OBJ_SSBMOD
- [x] SF_OBJ_FFT_1
- [x] SF_OBJ_FFT_2 (exists, but not available in Orion)
- [x] SF_OBJ_FFT_3 (exists, but not available in Orion)
- [ ] SF_OBJ_FFT_FORMANT (not used in Orion)
- [x] SF_OBJ_LOWCUT
- [x] SF_OBJ_ANALOG_EQ
- [x] SF_OBJ_DIFFDELAY1
- [x] SF_OBJ_DIFFDELAY2
- [x] SF_OBJ_HARD_LIMITER
- [x] SF_OBJ_NDISTORTION
- [ ] SF_OBJ_FREEDYNAMICS (not used in Orion)
- [x] SF_OBJ_NUGATE (exists, but not available in Orion?)
- [x] SF_OBJ_NULIMITER
- [x] SF_OBJ_NUCOMPRESSOR (exists, but not available in Orion?)
- [x] SF_OBJ_TREMOLO2
- [ ] SF_OBJ_SOMEFX (exists, but not available in Orion)
- [x] SF_OBJ_SD1_WRAPPER (Wrapper to Analog Distortion AD-1 VST effect)
- [x] SF_OBJ_NUPANNER
- [ ] SF_OBJ_EQ_TRACKING1 (not used in Orion)
- [x] SF_OBJ_EQ_TRACKING2
- [x] SF_OBJ_EQ_TRACKING4
- [ ] SF_OBJ_EQ_TRACKING6 (not used in Orion)
- [x] SF_OBJ_PEAKLIMIT
- [x] SF_OBJ_VINTAGE_EQ
- [x] SF_OBJ_HIGHCUT
- [x] SF_OBJ_NUCHORUS
- [ ] SF_OBJ_NUPHASER2 (not used in Orion)
- [x] SF_OBJ_DUALSSPLIT
- [x] SF_OBJ_FILTER_SALLEN_KEY (exists, but not available in Orion)

Since Orion 8:

- [x] SF_OBJ_2TAP_DELAY
- [ ] SF_OBJ_3TAP_DELAY (not used in Orion?)
- [ ] SF_OBJ_OCTAVER (not used in Orion?)
- [ ] SF_OBJ_PITCHSHIFT	(not used in Orion?)
- [ ] SF_OBJ_AMPCABINET	(not used in Orion?)
- [ ] SF_OBJ_NUDSTRTION (not used in Orion?)
- [ ] SF_OBJ_NUPHASER3 (not used in Orion?)	
- [ ] SF_OBJ_NUCHORUS2 (not used in Orion?)	
- [x] SF_OBJ_BRICKWALL	
- [ ] SF_OBJ_MULTIBLIM (not used in Orion?)	
- [x] SF_OBJ_X_DELAYPRO	
- [ ] SF_OBJ_DELAY2	(not used in Orion?)		
- [ ] SF_OBJ_DELAY3	(not used in Orion?)		
- [x] SF_OBJ_REVERB_R1	
- [ ] SF_OBJ_REVERB_R2 (not used in Orion?)	
- [x] SF_OBJ_RMM2

## Other Chunks

- [x] SF_HEADER (song version)
- [x] SF_GLOB (song info)
- [x] SF_USERBITMAP (song info image, skipped)
- [x] SF_EVTS (global events, skipped)
- [x] SF_PATC (playlist patterns, skipped)
- [x] SF_OBJS (global module count)
- [x] SF_SCENES (not used in Orion, it was feature for switchable pattern mode sets for live performance)
- [x] SF_WINP (window positions)
- [x] SF_FX_ASSIGNS (sends and Orion Pro master inserts)
- [x] SF_FX_RETURNS (master mixer)
- [x] SF_CHAN (mixer channels)
- [x] SF_AUDIO (audiotrack file references; supports bundle, but not used in Orion)
- [x] SF_MODULE_END
- [x] SF_EOF (stop reading)


## Notes regard Orionology app logo

The logo is based on the official Synapse Audio Orion logo. While the press kit license does not permit modifying the logo, I took the liberty of doing that anyway in the grounds that the refreshed logo was originally designed by me (Jouni Airaksinen) and the Orionology tool is sanctioned officially by Synapse Audio.

![Orionology](orionology_icon.png)
