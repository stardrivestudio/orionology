VERSION="$1"
export CARGO_TARGET_DIR=target
mkdir $CARGO_TARGET_DIR
cargo build --release

./make-dist.sh $CARGO_TARGET_DIR
./pack-dist.sh "mac-$VERSION"