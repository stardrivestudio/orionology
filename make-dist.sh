rm -rf dist
mkdir dist
cp $CARGO_TARGET_DIR/release/orionology dist/
cp LICENSE dist/
cp README.md dist/
cp orionology_icon.png dist/