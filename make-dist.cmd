rmdir /Q /S dist
mkdir dist
copy %CARGO_TARGET_DIR%\release\orionology.exe dist\
copy LICENSE dist\
copy README.md dist\
copy orionology_icon.png dist\