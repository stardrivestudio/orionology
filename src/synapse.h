 /* 
    Synapse Audio Orion SFS (Sonic Factory Song)
    
    Same data is used for Orion Pro and Orion Platinum files. Some data is just defaulted with Orion Pro when
    saving. Technically afair Orion Pro could load Platium version files and play them, with difference that
    all data was not editable and some effects/generators might have not been available.

    Orion Pro was later discontinued and Orion Platinum was rebranded around version 7 as simply Orion. 

    These types are directly from the Orion c++ source codes with permission by Richard Hoffmann from 
    Synapse Audio. I worked as freelancer programmer for Synapse Audio developing the Orion, so I have
    access and inside knownledge on the Orion source code.

	Notes:
	File could be splitted into more logical groups, now it's just one big copy/paste bunch

    Jouni Airaksinen
    jouni@kettureaktio.fi
*/

// Causes bindgen to make code with warnings in current rust compiler, let's not use unless really needed
//#pragma pack(1)

#include "windows.h"

// ========== SYNAPSE AUDIO ===========
// Filespec.h & Events.h

#define SF_HEADER			0x64684656
#define SF_GLOB				0x424F4C47

// note: jouni - bindgen did not convert char constants, manually converted into hex

#define SF_EVTS				0x53545645 //'STVE'		// 'EVTS' chunk (main event list)
#define SF_PATC				0x43544150 //'CTAP'		// 'PATC' chunk (pattern change list)
#define SF_WINP				0x504E4957 //'PNIW'		// 'WINP' chunk (window positions & state)
#define SF_AUDIO			0x4F495541 //'OIUA'		// 'AUIO' chunk (audio data)
#define SF_OBJS				0x534A424F //'SJBO'		// 'OBJS' chunk (objects)
#define SF_CHAN				0x4E414843 //'NAHC'		// 'CHAN' chunk (channels)
#define SF_FX_RETURNS		0x4E525846 //'NRXF'		// 'FXRN' chunk (fx return channels)
#define SF_SCENES			0x53434E53 //'SCNS'		// 'SNCS' chunk (scenes)
#define SF_USERBITMAP		0x504D4255 //'PMBU'		// 'UBMP' chunk (bitmap info+bitmap)
#define SF_MODULE_END		0x444F4D4F //'DOMO'		// 'OMOD' chunk (object end)
#define SF_FX_SENDS			0x55555555	// send & master fx, OLD versions
#define SF_FX_ASSIGNS		0x55555556	// send & master fx assignment header

#define SF_PAT				2000		// 1 pattern with x events follows
#define SF_PAT_COUNT		2001		// number of patterns in module, sent before SF_PAT
#define SF_PAT_ORDER		2002		// pattern order follows with x entries
#define SF_CONTROLS			2003		// control data (positions, status...)
#define SF_CHAN_INFO		2004		// chan name,...
#define SF_PAT_LENGTH		2005		// pattern length in ticks
#define SF_GEN_INFO			2006		// name, filename(vst)...
#define SF_FX_INFO			2007		// name, filename(vst)...
#define SF_WT_INFO			2007		// wavetable info
#define SF_GEN_DATA			2007		// generator data

#define SF_SMP_POOL			0
#define SF_SMP_HEADER		1
#define SF_SMP_DATA			2
#define SF_PROGRAM			3			// program/patch name
#define SF_PROGRAM_ID		4			// program/patch ID
#define SF_TRACK_DATA		5			// drum module or VST track data

#define SF_OBJ_SAMPLER		0x1000		// tone generators
#define SF_OBJ_WASP			0x1001
#define SF_OBJ_909			0x1002
#define SF_OBJ_VST2SYNTH	0x1003
#define SF_OBJ_303			0x1004
#define SF_OBJ_WAVE			0x1005
#define SF_OBJ_AUDIO		0x1006
#define SF_OBJ_DRUMS		0x1007
#define SF_OBJ_MIDI			0x1008
#define SF_OBJ_TOMKICK		0x1009
#define SF_OBJ_HIHATS		0x100A
#define SF_OBJ_SCREAMER		0x100B
#define SF_OBJ_PLUCKED		0x100C
#define SF_OBJ_AUTOMATION	0x100D		// automation track
#define SF_OBJ_ULTRAN		0x100E
#define SF_OBJ_REWIRE		0x100F
#define SF_OBJ_FMSYNTH		0x1010
#define SF_OBJ_GRAND		0x1011
#define SF_OBJ_DXSYNTH		0x1012
#define SF_OBJ_P8			0x1013 // reserved
#define SF_OBJ_P9			0x1014
#define SF_OBJ_LASTGEN		0x10FF

#define SF_OBJ_FIRSTFX		0x1100		// effects
#define SF_OBJ_DELAY		0x1100
#define SF_OBJ_REVERB		0x1101
#define SF_OBJ_PHASER		0x1102
#define SF_OBJ_DISTORTION	0x1103
#define SF_OBJ_VST			0x1104
#define SF_OBJ_COMPRESSOR	0x1105
#define SF_OBJ_TRANCEGATE	0x1106
#define SF_OBJ_DELAY_STEREO	0x1107
#define SF_OBJ_CHORUS		0x1108
#define SF_OBJ_PCF			0x1109
#define SF_OBJ_ULTRAVERB	0x110A
#define SF_OBJ_X_DELAY		0x110B
#define SF_OBJ_TS9DIST		0x110C
#define SF_OBJ_DX			0x110D
#define SF_OBJ_FILTER		0x110E
#define SF_OBJ_NUPHASER		0x110F
#define SF_OBJ_FLANGER		0x1110		// UNUSED
#define SF_OBJ_FILTERDELAY	0x1111
#define SF_OBJ_ECHO			0x1112
#define SF_OBJ_ULTRAVERB2	0x1113
#define SF_OBJ_PLATE_REVERB	0x1114
#define SF_OBJ_RINGMOD		0x1115
#define SF_OBJ_ANALYZER		0x1116		// FUTURE USE
#define SF_OBJ_TALKBOX		0x1117		// FUTURE USE
#define SF_OBJ_GATE			0x1118
#define SF_OBJ_CLIPPER		0x1119
#define SF_OBJ_VOCODER		0x111A		// FUTURE USE
#define SF_OBJ_PARAM_EQ		0x111B
#define SF_OBJ_LIMITER		0x111C
#define SF_OBJ_LATENCY		0x111D		// latency add fx
#define SF_OBJ_CHORUS2		0x111E
#define SF_OBJ_STEREOWIDEN	0x111F
#define SF_OBJ_PLAT_ECHO	0x1120
#define SF_OBJ_TREMOLO		0x1121
#define SF_OBJ_LESLIE		0x1122
#define SF_OBJ_VOLUME		0x1123
#define SF_OBJ_HARM_FILTER	0x1124		// harmonic resonator
#define SF_OBJ_FILTER_24	0x1125		// variable filter
#define SF_OBJ_BAND_SPLIT	0x1126		// band split fx
#define SF_OBJ_HARM_DELAY	0x1127		// UNUSED
#define SF_OBJ_REVERB3		0x1128		// platinum reverb
#define SF_OBJ_FXCONTAINER	0x1129
#define SF_OBJ_LOFI			0x112A
#define SF_OBJ_COMP_MULTI	0x112B
#define SF_OBJ_SSBMOD		0x112C		// single sideband modulator
#define SF_OBJ_FFT_1		0x112D		// FUTURE USE
#define SF_OBJ_FFT_2		0x112E		// FUTURE USE
#define SF_OBJ_FFT_3		0x112F		// FUTURE USE
#define SF_OBJ_FFT_FORMANT	0x1130		// Formant shift
#define SF_OBJ_LOWCUT		0x1131		// Low cut etc filter
#define SF_OBJ_ANALOG_EQ	0x1132
#define SF_OBJ_DIFFDELAY1	0x1133
#define SF_OBJ_DIFFDELAY2	0x1134
#define SF_OBJ_HARD_LIMITER	0x1135
#define SF_OBJ_NDISTORTION	0x1136
#define SF_OBJ_FREEDYNAMICS	0x1137
#define SF_OBJ_NUGATE		0x1138
#define SF_OBJ_NULIMITER	0x1139
#define SF_OBJ_NUCOMPRESSOR	0x113A		// FUTURE USE
#define SF_OBJ_TREMOLO2		0x113B		// Tremolo
#define SF_OBJ_SOMEFX		0x1140		// TEST USE
#define SF_OBJ_SD1_WRAPPER	0x1141
#define SF_OBJ_NUPANNER		0x1142
#define SF_OBJ_EQ_TRACKING1	0x1143
#define SF_OBJ_EQ_TRACKING2	0x1144
#define SF_OBJ_EQ_TRACKING4	0x1145
#define SF_OBJ_EQ_TRACKING6	0x1146
#define SF_OBJ_PEAKLIMIT	0x1147
#define SF_OBJ_VINTAGE_EQ	0x1148
#define SF_OBJ_HIGHCUT		0x1149		// FUTURE USE
#define SF_OBJ_NUCHORUS		0x114A		// vintage chorus
#define SF_OBJ_NUPHASER2	0x114B
#define SF_OBJ_DUALSSPLIT	0x114C
#define SF_OBJ_FILTER_SALLEN_KEY 0x114D
// Orion 8 -->
#define SF_OBJ_2TAP_DELAY	0x114E
#define SF_OBJ_3TAP_DELAY	0x114F
#define SF_OBJ_OCTAVER		0x1150
#define SF_OBJ_PITCHSHIFT	0x1151
#define SF_OBJ_AMPCABINET	0x1152
#define SF_OBJ_NUDSTRTION	0x1153
#define SF_OBJ_NUPHASER3	0x1154
#define SF_OBJ_NUCHORUS2	0x1155
#define SF_OBJ_BRICKWALL	0x1156
#define SF_OBJ_MULTIBLIM	0x1157
#define SF_OBJ_X_DELAYPRO	0x1158
#define SF_OBJ_DELAY2		0x1159
#define SF_OBJ_DELAY3		0x115A
#define SF_OBJ_REVERB_R1	0x115B
#define SF_OBJ_REVERB_R2	0x115C
#define SF_OBJ_RMM2			0x115D
#define SF_OBJ_FXTEST		0x11FE		// for experimental purposes
#define SF_OBJ_LASTFX		0x11FF


#define SF_OBJ_NAME			0x1500		// object name

#define SF_OBJ_END			0xFFFFFFFE	// end of object

#define SF_EOF				0xFFFFFFFF	// end of file (last event)

// data dword event
typedef struct tagSF_EVTDWORD
{
	DWORD	id;			// ID
	DWORD	data;		// data
} SF_EVTDWORD;

// data float event
typedef struct tagSF_EVTFLOAT
{
	DWORD	id;			// ID
	float	data;		// data
} SF_EVTFLOAT;

typedef struct tagWindowPosInfo
{
	RECT	rect;
	BYTE	bInfoValid;
	BYTE	bMinimized;
	BYTE	bHidden;		// window is present but hidden
	BYTE	bFullRect;		// use full rectangle (mixer), or only x/y position (generators)
} WindowPosInfo;

// grid setup structure
typedef struct tagGridCfg
{
	BYTE	bSteps;					// step subdivisions (def: 16)
	BYTE	bHilite;				// hilite every bHilite line
	BYTE	bSnapToGrid;			// currently on(1) or off(0)
	BYTE	bZoomLevelX;			// X zoom index 0..x 
	BYTE	bZoomLevelY;			// Y zoom index 0..x
	BYTE	bFlags;					// flags (see above)
	BYTE	bPosX;					// x-position in grid
	BYTE	bPosY;					// y position in grid
} GridCfg;

typedef struct tagIntonationTable
{
	double	values[128];
	char	name[128];
	int		nCount;
} IntonationTable;

typedef struct tagOldIntonationTable
{
	double	values[12];
	char	name[128];
} OldIntonationTable;

// 76-byte fx header
typedef struct tagFXHeader
{
	DWORD	dwFlags;				// header flags
	char	achName[40];			// name of plugin that appears in window title
	int		iChans;					// mono=1, stereo=2
	int		bAssigned;				// assigned somewhere = TRUE
	char	bSidechain;				// sidechain channel, -1 = off
	char	bMidiSidechain;			// MIDI sidechain channel, -1 = off
	BYTE	bReserved[2];
	int		iReserved[4];			// reserved, must be 0
	char	achPrsName[40];			// preset name
} FXHeader;

// (custom) control types
#define	CTRL_WHEEL			0		// wheel
#define	CTRL_TOGGLE_BT		1		// toggle button
#define	CTRL_PUSH_BT		2		// push button
#define	CTRL_RADIO_BT		3		// radio button array
#define	CTRL_CUSTOM			4		// custom control (drawn by derived class)
#define	CTRL_SLIDER			5		// slider
#define CTRL_BT_TRANS		6		// simple transparent button
#define CTRL_UNKNOWN		7		// static or push button
#define	CTRL_SLIDER_VERT	8		// vertical slider
#define CTRL_FXCONTAINER	9		// special control for effect container
#define CTRL_2D_FIELD		10		// special 2d control
#define CTRL_FREQSPLIT		11		// special frequency split control
#define CTRL_FREEHAND		12		// special freehand drawing control /fx

// control flags
#define CTRL_AUTOMATION		0x0		// recordable controls (default)
#define CTRL_IS_BUTTON		0x1		// buttons
#define CTRL_NOPERSIST		0x2		// non-persistent controls
#define CTRL_SPEED_HALF		0x4		// half-speed controls
#define CTRL_NOAUTO			0x8		// unused

// static/runtime control data
typedef struct tagControlProp
{
	int		bHiddenParam;			// hidden control parameter, automation skipped
} CtlProp;

/*
typedef struct tagControlInfo
{
	int		x1, y1;					// position left/top
	int		x2, y2;					// position right/bottom
	void*	pCustomData;			// ctrl-specific pointer
	int		iData;					// ctrl-specific data
	int		iCtrlType;				// controller type (wheel, toggle button, ...)
	int		iRangeFrom;				// parameter range lower bound
	int		iRangeTo;				// parameter range upper bound (0 = float range [0,1])
	int		nFlags;					// for CTRL_SPEED_HALF, and also OSIRIS.
	char	achName[32];			// parameter name, max. 31 chars + 0-byte
} CtlInfo;
*/

//#pragma pack(push, 1)
// controller data that changes & needs to be persistent
typedef struct tagCtlData
{
	int		iCtrlValue;				// current controller value in [0, 128]
	int		iInitialVal;			// initial ctl value before it's recorded
	int		iMidiCtrl;				// assigned midi controller (0=default)
	int		iCookedValue;			// cooked parameter value
	BYTE	bCtlStat;				// 0=not recorded, 1=just recorded, 2=recorded & #0-stuffed
	BYTE	bHidden;				// 0=visible, 1=invisible
} CtlData;

// controller data, floating point representation
typedef struct tagControlDataFloat
{
	float	fCtrlValue;				// current controller value in [0,1]
	float	fInitialVal;			// initial ctl value before it's recorded
	int		iMidiCtrl;				// assigned midi controller
	float	fCookedValue;			// cooked parameter value
	BYTE	bCtlStat;				// 0=not recorded, 1=just recorded, 2=recorded & #0-stuffed
	BYTE	bReserved;
} CtlDataFloat;
//#pragma pack(pop)


typedef struct tagSONGINFO
{
	int	bShowInfo;		// show info on song open
	DWORD	dwLenGeneral;	// string lengths
	DWORD	dwLenURL;
	DWORD	dwReserved;		// must be 0
	// strings follow
} SONGINFO;

typedef struct tagGlobCfg
{
	DWORD		dwFlags;
	GridCfg		playlistCfg;
	float		fTempoBpm;
	DWORD		dwTicksPerBar;
	int			iLoopStart;
	int			iLoopEnd;
	int			lMaster;
	short		iSidechainChan1;
	short		iSidechainChan2;
	WindowPosInfo playlistPos;
	int			nClipMode;
	WindowPosInfo masterPos;
	int			nSongLength;
	int			nTuningTable;	// 0..n-1
	int			nTuningRootKey;
	IntonationTable Table;
	int			nStepsPerBar;
	DWORD		nDefaultSteps;
} GlobCfg;

// Used prior version xxx
typedef struct tagOldGlobCfg
{
	DWORD		dwFlags;
	GridCfg		playlistCfg;
	float		fTempoBpm;
	DWORD		dwTicksPerBar;
	int			iLoopStart;
	int			iLoopEnd;
	int			lMaster;
	short		iSidechainChan1;
	short		iSidechainChan2;
	WindowPosInfo playlistPos;
	int			nClipMode;
	WindowPosInfo masterPos;
	int			nSongLength;
	int			nTuningTable;	// 0..n-1
	int			nTuningRootKey;
	IntonationTable Table;
	int			nStepsPerBar;
} OldGlobCfg;

// Used prior version xxx
typedef struct tagVeryOldGlobCfg
{
	DWORD		dwFlags;
	GridCfg		playlistCfg;
	float		fTempoBpm;
	DWORD		dwTicksPerBar;
	int			iLoopStart;
	int			iLoopEnd;
	int			lMaster;
	short		iSidechainChan1;
	short		iSidechainChan2;
	WindowPosInfo playlistPos;
	int			nClipMode;
	WindowPosInfo masterPos;
	int			nSongLength;
	int			nTuningTable;	// 0..n-1
	int			nTuningRootKey;
	OldIntonationTable Table;
	int			nStepsPerBar;
} VeryOldGlobCfg;

typedef struct tagFxAssignHeader
{
	short	sReserved;
	BYTE	bSends;
	BYTE	bMaster;
	short	iSendID[4];			// assigned module ID 0..n-1, max # need to be 
	short	iMasterID[4];		// constantly 4 to be compatible across all versions
} FxAssignHeader;

// Channel.h

// number of controls/fx sends
#define FX_SENDS		4
#define FX_MASTERCHANS	13
#define FX_MASTERCHANS_PRE_8000 9
#define FX_MASTERCHANS_PRE_3100 4

#define CHANFLAGS_IS_STEREO		0x01
#define CHANFLAGS_EQS_EXTENDED	0x02	// EQs have twice the range since V2.1
#define CHANFLAGS_EQS_CLIPPED	0x04
#define CHANFLAGS_EQS_LO12		0x08	// 12db instead of 6dB lowshelf filter
#define CHANFLAGS_EQS_HI12		0x10	// 12db instead of 6dB hishelf filter
#define CHANFLAGS_SEND_LINK		0x20	// send volume linked to channel volume
#define CHANFLAGS_EQS_POSTFX	0x40	// EQ comes after effects (def: before!)

// channel file header
typedef struct tagChanInfo
{
	char	achName[24];	// channel label
	int		iInsert1;		// module index fx-1, none=-1
	int		iInsert2;		// module index fx-2, none=-1
	int		iStereoLink;	// link for double mono pairs
	DWORD	dwFlags;		// flags (see above)
	int		bIsStereoChan;	// stereo channel
	float	fLoShelfFreq;	// reserved
	float	fHiShelfFreq;	// reserved
	BYTE	bSendLink[4];	// reserved
	int		iInsert3;		// module index fx-3, none=-1
	int		iInsert4;		// module index fx-4, none=-1
	WORD	nModule;		// module ID this channel belongs to
	BYTE	nReserved[2];	// reserved
} ChanInfo;

// OrionDoc.h

#define MAX_MODULES			255			// maximum # of modules (warning, should not exceed BYTE boundaries currently!!)
#define MAX_CHANNELS		512			// maximum # of channels
#define MAX_LIST_NOTES		200			// maximum # of notes in triggered note list
#define DEF_EVENT_BLK		8192		// minimum # of free events in recorded event list

#define FX_MAIN_INSERTS		4
#define FX_CHANNEL_INSERTS	2
#define FX_MASTER_INSERTS	4
#define	SUB_BUSSES			4

// Event.h

//
// global events [$1000, $3000)
//
#define EVENT_GLOBAL_FIRST	0x1000
#define NUM_GLOBAL_EVENTS	5

#define EVENT_TEMPO			0x1000		// common events, affecting all tracks
#define EVENT_BARSIZE		0x1001
#define EVENT_VOLUME		0x1002		// global volume
#define EVENT_PATCHANGE		0x1003		// OUTDATED pattern change
#define EVENT_GEN_MUTE		0x1004		// mute generator (= disable note receival)
#define EVENT_GEN_SOLO		0x1005		// reserved
#define EVENT_PAN			0x1006

#define	EVENT_SONGMODE		0x1100		// settings (non-recordable, not in event list)
#define	EVENT_LOOP_ON		0x1101
#define	EVENT_LOOP_START	0x1102
#define	EVENT_LOOP_END		0x1103
#define	EVENT_SONGINFO		0x1104

//
// common module events [$3000, $5000)
//
 
#define EVENT_COMMON_FIRST	0x3000		// start

#define EVENT_NOTE_ON		0x3000
#define EVENT_NOTE_OFF		0x3001
#define EVENT_PAT_NOTE		0x3002
#define EVENT_SLIDE			0x3003
#define EVENT_ACCENT		0x3004
#define EVENT_PITCHBEND		0x3005
#define EVENT_PAT_NOTEPROB	0x3006		// note probability
#define EVENT_PAT_CHANGE	0x3007		// pattern change
#define EVENT_PAT_SLIDE		0x3008
#define EVENT_MIDI			0x3009		// midi event
#define EVENT_SETPROGRAM	0x300A		// program change
#define EVENT_SUSTENUTO		0x300B		// sustenuto pedal
#define EVENT_EXPRESSION	0x300C		// expression pedal
#define EVENT_GATE			0x300D

// transport events
#define EVENT_PLAY			0x3010		// play
#define EVENT_STOP			0x3011		// stop
#define EVENT_PAUSE			0x3012		// pause
#define EVENT_REPOSITION	0x3013		// reposition song pointer
#define EVENT_ALLNOTESOFF	0x3014		// all notes off

//
// mixer channel events  [$5000, $FFFF]
//
 
#define EVENT_CH_FIRST		0x5000		// events start from here

#define EVENT_CH_VOLUME		0x0000
#define EVENT_CH_PAN		0x0001
#define EVENT_CH_FX1AMT		0x0002
#define EVENT_CH_FX2AMT		0x0003
#define EVENT_CH_FX3AMT		0x0004
#define EVENT_CH_FX4AMT		0x0005
#define EVENT_CH_EQ_ON		0x0006
#define EVENT_CH_EQ_LOSHELF	0x0007
#define EVENT_CH_EQ_HISHELF	0x0008
#define EVENT_CH_EQ_GAIN1	0x0009
#define EVENT_CH_EQ_FREQ1	0x000A
#define EVENT_CH_EQ_Q1		0x000B
#define EVENT_CH_EQ_GAIN2	0x000C
#define EVENT_CH_EQ_FREQ2	0x000D
#define EVENT_CH_MUTE		0x000E
#define EVENT_CH_SOLO		0x000F
#define EVENT_CH_FX5AMT		0x0010		// fx send 3
#define EVENT_CH_FX6AMT		0x0011		// fx send 4
#define EVENT_CH_FX7AMT		0x0012		// fx insert 3
#define EVENT_CH_FX8AMT		0x0013		// fx insert 4
#define EVENT_CH_BUS1		0x0014		// bus 1
#define EVENT_CH_BUS2		0x0015		// bus 2
#define EVENT_CH_BUS3		0x0016		// bus 3
#define EVENT_CH_BUS4		0x0017		// bus 4
#define EVENT_CH_BUS5		0x0018		// bus 5
#define EVENT_CH_EQ_POSTFX	0x0019		// pre/post EQ
#define EVENT_CH_EQ_Q2		0x001A		// Q from Low Mid EQ
#define EVENT_CH_FRONT_AMT	0x001B		// frontct
#define EVENT_CH_REAR_AMT	0x001C		// rearct
#define EVENT_CH_LFE_AMT	0x001D		// LFEct

#define EVENT_NOT_RECORDED	0
#define EVENT_RECORDED		1
#define EVENT_AT_TICK0		2

// "basic" event struc
typedef struct tagEvent  
{
	WORD	wModNum;	// module or channel # counting from 0, -1 for global events
	WORD	wId;		// event ID
	DWORD	dwTime;		// tick time (inside pattern or inside song)
	int		iData;		// e.g. controller event data
	DWORD	dwExtra;	// in song events, used to distinguish master/mixer channels
} Event;

// fast access (channel/ID is DWORD)
typedef struct tagEventFast
{
	DWORD	dwModId;	// module # and ID
	DWORD	dwTime;		// tick time (inside pattern or inside song)
	int		iData;		// e.g. controller event data
	DWORD	dwExtra;	// currently used by EventChannel to distinguish master/mixer channels
} EventFast;

// pattern access
typedef struct tagEventPattern
{
	WORD	wModNum;	// module or channel # counting from 0, -1 for global events
	WORD	wId;		// event ID
	DWORD	dwTime;		// tick time (inside pattern or inside song)
	int		iData;		// e.g. controller event data
	BYTE	bReserved[2];// reserved
	BYTE	bInstr;		// instrument # (counting from 0)
	BYTE	bIsSelected;// selection state
} EventPattern;

// 16-byte note event
typedef struct tagEventNote
{
	BYTE	bRes0;		// reserved for future use
	BYTE	bRes;		// reserved for future use
	WORD	wId;		// event ID
	DWORD	dwTime;		// note start in ticks (relative to pattern)
	DWORD	dwLenTicks;	// length in ticks
	BYTE	bNote;		// pitch
	BYTE	bVolume;	// volume
	BYTE	bInstr;		// instrument # (counting from 0)
	BYTE	bIsSelected;// selection state
} EventNote;

// 20-byte note event
typedef struct tagEventNoteXT
{
	BYTE	bRes0;		// probability (OSIRIS only)
	BYTE	bRes;		// reserved for future use
	WORD	wId;		// event ID
	DWORD	dwTime;		// note start in ticks (relative to pattern)
	DWORD	dwLenTicks;	// length in ticks
	BYTE	bNote;		// pitch
	BYTE	bVolume;	// volume
	BYTE	bInstr;		// instrument # (counting from 0)
	BYTE	bIsSelected;// selection state
	DWORD	dwDeltaFrames;// for Cubase engine type
} EventNoteXT;

// special data split for channel events
typedef struct tagEventChannel
{
	WORD	wChanNum;	// channel # counting from 0
	WORD	wId;		// event ID
	DWORD	dwTime;		// tick time (inside pattern or inside song)
	short	iData;		// data
	short	zero;
	BYTE	bIsMaster;	// mixer channels: 0 master channels: 1
	BYTE	bReserved[3];// reserved
} EventChannel;

// float event
typedef struct tagEventFloat
{
	WORD	wModNum;	// module # counting from 0 (-1 for global events)
	WORD	wId;		// event ID
	DWORD	dwTime;		// tick time (inside pattern or inside song)
	float	fData;
	DWORD	dwReserved; // unused
} EventFloat;

// MIDI event
typedef struct tagEventMIDI
{
	WORD	wModNum;	// module # counting from 0 (-1 for global events)
	WORD	wId;		// EVENT_MIDI
	DWORD	dwTime;		// tick time (inside pattern or inside song)
	BYTE	bData[4];
	DWORD	dwReserved; // unused
} EventMidi;

// pattern change event
typedef struct tagEventPatChange
{
	WORD	wModNum;	// module # counting from 0
	WORD	wId;		// event ID (here: EVENT_PATCHANGE)
	DWORD	dwTime;		// tick time (inside pattern or inside song)
	short	sData;		// pattern to change to
	BYTE	bIsSelected;// selection state (0=not selected)
	BYTE	bVstIndex;	// VST channel index
	DWORD	dwReserved; // unused
} EventPatChange;

// audio event
typedef struct tagAudioEventOld
{
	DWORD	dwLength;		// length in ticks
	DWORD	dwTime;
	short	nAudioSnippet;	// audio wav ID (0..n-1)
	BYTE	bIsSelected;	// selection state (0=not selected)
	BYTE	bReserved;		// unused
	DWORD	dwReserved;		// unused
} VeryOldAudioEvent;

struct OldAudioEvent
{
	DWORD	dwLengthOLD;	// length in ticks, older versions
	DWORD	dwTimeOLD;		// event start time in ticks, older versions
	short	nAudioSnippet;	// audio wav ID (0..n-1)
	BYTE	bIsSelected;	// selection state (0=not selected)
	BYTE	bModID;			// mod index
	int		nSampleStart;	// trim start
	int		nSampleEnd;		// trim end
	BYTE	bIsUnique;		// 1 if unique
	BYTE	bReserved3[3];
	double	dTime;			// event start time in ticks
	double	dLength;		// length in ticks
	double	dFactor;		// clip sampling rate vs project sampling rate (1.0 means they're equal)
	int		nSelStart;
	int		nSelEnd;
	DWORD	dwReserved[2];
};

struct AudioEvent
{
	DWORD	dwLengthOLD;	// length in ticks, older versions
	DWORD	dwTimeOLD;		// event start time in ticks, older versions
	short	nAudioSnippet;	// audio wav ID (0..n-1)
	BYTE	bIsSelected;	// selection state (0=not selected)
	BYTE	bModID;			// mod index
	int		nSampleStart;	// trim start
	int		nSampleEnd;		// trim end
	BYTE	bIsUnique;		// 1 if unique
	BYTE	bReserved3[3];
	double	dTime;			// event start time in ticks
	double	dLength;		// length in ticks
	double	dFactor;		// clip sampling rate vs project sampling rate (1.0 means they're equal)
	int		nSelStart;
	int		nSelEnd;
	float	dAttack[2];
	float	dRelease[2];
	DWORD	dwReserved[6];
};

//////////////// obsolete

// old 12-byte note
typedef struct tagOldPatNote
{
	BYTE	bInstr;		// instrument # (counting from 0)
	BYTE	bIsSelected;// selection state
	WORD	wId;		// event ID
	DWORD	dwTime;		// note start in ticks (relative to pattern)
	BYTE	bNote;		// pitch
	BYTE	bVolume;	// volume
	WORD	wLenTicks;	// length in ticks
} OldPatNote;

// old 12-byte event
typedef struct tagOldEvent
{
	WORD	wModNum;	// module # counting from 0 (-1 for global events)
	WORD	wId;		// event ID
	DWORD	dwTime;		// tick time (inside pattern or inside song)
	int		iData;		// e.g. controller event data
} OldEvent;


// Generator.h
#define GENFLAGS_STEREOCHAN			0x00000001	// stereo channel used by generator
#define GENFLAGS_VST2CHUNK			0x00000002	// vst2 synth uses chunk saving
#define GENFLAGS_ARP_ON				0x00000004	// arpeggiator on
#define GENFLAGS_AUTOCHORD_BIT1		0x00000008	// autochord mode bit 1
#define GENFLAGS_DRUMS_BASIC		0x00000010	// drum module, show basic params
#define GENFLAGS_DRUMS_EXT			0x00000020	// drum module, show extended params
#define GENFLAGS_DRUMS_VEL			0x00000040	// drum module, show velocity routing params
#define GENFLAGS_DRUMS_STEPS		0x00000080	// drum module, show steps
#define GENFLAGS_DRUMS_V2			0x00000100	// drum module v2 whichs supports visibility
#define GENFLAGS_PLAYLISTTRACKS		0x00000200	// vst2 synth multitrack info present
#define GENFLAGS_ARP_RESET			0x00000400	// arp reset flag
#define GENFLAGS_MIDI_AUTOMATION	0x00000800	// cubase MIDI automation
#define GENFLAGS_ARP_RANDOM			0x00001000	// Arp random mode
#define GENFLAGS_STEPSEQ_USED		0x00002000	// Step seq was used
#define GENFLAGS_EVTEDITOR_USED		0x00004000	// Step seq was used
#define GENFLAGS_DRUMS_MONOTIMBRAL	0x00008000	// drum module v3 monotimbral switch
#define GENFLAGS_AUTOCHORD_BIT2		0x00010000	// autochord mode bit 2
#define GENFLAGS_RECORD_ON			0x00020000	// record/monitor in playlist activated
#define GENFLAGS_SOLO_ON			0x00040000	// record/monitor in playlist activated
#define GENFLAGS_MUTE_ON			0x00080000	// record/monitor in playlist activated
#define GENFLAGS_VST2COMPLETEBANK	0x00100000	// complete VST banks, for non-chunked pluginz

// playlist track header
typedef struct tagPlaylistTrack
{
	char		achName[32];		// name of track
	COLORREF	color;				// track color
	BYTE		bMuteOn;
	BYTE		bSoloOn;
	BYTE		bAudioIn;			// audio in channel
	BYTE		bxx2;				// reserved
	DWORD		dwReserved[6];		// reserved for future use
} PlaylistTrack;

// object header
typedef struct tagGenHeader
{
	DWORD		dwFlags;				// flags for this generator
	char		achName[40];			// generator name
	int			iMaxVoices;
	int			iMidiChan;
	int			iQuality;
	int			iPattern;				// for pattern mode
	GridCfg		gridCfg;				// config for piano roll
	BYTE		bArpRange;				// arp range, 0..4 (1..5 octaves)
	BYTE		bArpOrder;				// arp playback order (0..n-1)
	BYTE		bArpTempo;				// arp note tempo (0=Quarter,1=Sixteenth...)
	BYTE		bArpGate;				// arp gate
	char		cArpDeltas[16];
	BYTE		bChordType[16];
	BYTE		bChordKey[16];
	BYTE		bNumChords;
	BYTE		bNumArpDeltas;
	BYTE		bSidechainChan;			// 0=OFF
	BYTE		bGenVersion;			// some version number 0..255, default 0
	DWORD		dwArpStep;				// arp step array, max. 32 on/off steps
	int			nLastEvtEdited;			// last event edited in piano roll
	char		achPresetName[40];		// preset used
	BYTE		bMidiSidechain;			// default 0
	BYTE		bMidiPbRange;			// old songs have 0 here and must be set to 12
	BYTE		bChannelCount;			// # of output channels (since 4.6!)
	BYTE		bLoKey;
	BYTE		bHiKey;
	BYTE		bArpTrigger;
	BYTE		bArpDistance;
	BYTE		bArpRepeats;
	BYTE		bLivePatterns[28];
	int			iMidiPortNum;
	int			iChannelsVisible;		// visible channels. VSTi only for now.
} GenHeader;

// Sampler.h
// program type IDs
#define PROGRAMTYPE_SF2		0
#define PROGRAMTYPE_KRZ		1
#define PROGRAMTYPE_WAV		2
#define PROGRAMTYPE_ORION	3
#define PROGRAMTYPE_AKAI	4
#define PROGRAMTYPE_TXT		5

#define SAMPLER_FLAGS_BUNDLE			0x00000001
#define SAMPLER_FLAGS_FX_OVERRIDE		0x00000002

typedef struct tagSampHdr
{
	DWORD	dwFlags;		// 1=BUNDLE, 2=FX OVERRIDE
	int		iPrgType;		// PROGRAMTYPE_WAV, ...
	int		iSelProg;		// selected program
	DWORD	dwBundleSize;	// data size of attached file
	DWORD	dwCompression;	// compression amount (UNUSED)

	// for OGG compression
	DWORD	dwSamplesUnpackedSizeBytes;
	DWORD	dwSamplesPackedSizeBytes;
	DWORD	dwReserved1;
	BYTE*	pSampleDataPacked;
	DWORD	dwReserved2[2]; //align to 16-byte boundary
//#ifndef _WIN64
	int		reserved[1];
//#endif
} SmpFileHdr;

// Vst2Synth.h

// object header
typedef struct tagVst2Header
{
	DWORD		dwFlags;				// header flags
	char		achName[40];			// name of plugin
	int			iMidiChan;
	int			iPattern;				// for pattern mode
	GridCfg		gridCfg;				// config for piano roll
	// jouni - I had to change long -> int to generate proper sized/aligned struct in rust 64-bit for 32-bit songs
	int			lNumProgram;			// selected program
	int			nPathLen;				// pathlen in bytes, string follows after header
	int			nNumTracks;				// # of tracks used in playlist
	int			nNumChannels;			// plug saved with n channels
	BYTE		bIsStereoChan[128];		// channel pins (mono or stereo)
	int			lPrograms;				// V8: number of programs stored
} Vst2Header;

typedef struct tagVstTrack
{
	int			bVstPatOn;				// multitimbral vst pattern playing
	int			bVstPattern;			// multitimbral vst pattern #
	DWORD		dwVstPos;				// multitimbral vst pattern pos
	DWORD		dwVstLastEvIndex;		// multitimbral vst pattern evt pos
} VstTrack;

// VstEffect.h

// vst fx header
typedef struct tagVstFxHeader
{
	DWORD	dwFlags;				// header flags
	char	achName[40];			// name of plugin that appears in window title
	int		iChans;					// mono=1, stereo=2
	int		bAssigned;				// assigned somewhere = TRUE
	// jouni - I had to change long -> int to generate proper sized/aligned struct in rust 64-bit for 32-bit songs
	int		lNumProgram;			// selected program
	int		nPathLen;				// pathlen followed by string
	BYTE	bMidiSidechain;
	BYTE	bBundle;				// 0 = OFF
	BYTE	bReserved[6];
} VstFxHeader;

// AudioTrack.h

#define AUDIOTRACK_ATTENUATION			0.5f
#define OLDAUDIOTRACK_ATTENUATION		0.25f
#define VERYOLDAUDIOTRACK_ATTENUATION	1.0f

// DrumModule.h & DrumModule.cpp

#define DM_TRACK_CTRLS			10
#define DM_TRKS					18						// max tracks
#define DM_CTRLS				DM_TRACK_CTRLS*DM_TRKS	// max # of controls

typedef struct tagNonRecData
{
	BYTE		bReverse;
	BYTE		bStretch;			// stretch to bar
	BYTE		bOutput;
	BYTE		bShuffleMode;
	BYTE		bAsBundle;
	char		bCutTrack;
	BYTE		bLoopOn;
	BYTE		bTrackVersion;
	DWORD		nLen;				// sample path length, or sample info struct length (bundled ver)
	// >= version 0x4200
	int			nFitToLen;
	short		nSubSamples;
	BYTE		bCutSwitch;
	BYTE		bMonoMulti;			// mono or multi mode
	BYTE		bRandomMode;		// random sample selection on or off
	BYTE		future[99];
} NonRecData;

typedef struct tagDrumHeader
{
	DWORD	dwFlags;
	DWORD	dwReserved[5];
} DrumHeader;

struct SampleInfoDrumsOld
{
	PCMWAVEFORMAT	m_wavHeader;	// WAV header
	int				reserved1;
	DWORD			m_dwStart;		// sample start, frames
	DWORD			m_dwEnd;		// sample end, frames
	DWORD			m_dwLoopStart;  // loop start, frames
	DWORD			m_dwLoopEnd;	// loop end, frames
	BYTE			m_bLoopSwitch;  // 0 = OFF, 1 = Fwd Loop
	BYTE			m_bRootKey;		// root key (used by AKP)
	BYTE			m_bDontDelete;	// dont delete right half of deinterlaced sample (used by AKP)
	BYTE			m_bModified;	// modified flag (usually 0)
	char			m_achPath[256];	// file path, limited to 235 chars + 0-byte
	BYTE			m_bLowVel;		// lower velocity this sample responds to
	BYTE			m_bHighVel;		// higher velocity this sample responds to
};

// Module.h
// sample info structure (v1)- now identical to SampleInfoDrums 
// Small changes regards m_pWaveData which is internal pointer and in different position due pointer size difference
// in 32-bit vs 64-bit environments. Reader(s) should omit the values as they used to point to a memory location..
struct SampleInfo
{
	PCMWAVEFORMAT	m_wavHeader;	// WAV header
	unsigned int	m_pWavData_ptr32_internal;	// raw sample data
	DWORD			m_dwStart;		// sample start, frames
	DWORD			m_dwEnd;		// sample end, frames
	DWORD			m_dwLoopStart;  // loop start, frames
	DWORD			m_dwLoopEnd;	// loop end, frames
	BYTE			m_bLoopSwitch;  // 0 = OFF, 1 = Fwd Loop
	BYTE			m_bRootKey;		// root key (used by AKP)
	BYTE			m_bDontDelete;	// dont delete right half of deinterlaced sample (used by AKP)
	BYTE			m_bModified;	// modified flag (usually 0)

	BYTE			m_bLowVel;		// lower velocity this sample responds to
	BYTE			m_bHighVel;		// higher velocity this sample responds to
	char			m_reserved[54];	// filler to match other sampleinfo structures :)
	unsigned long long int m_pWavData_ptr64_internal; // raw sample data (64-bit pointer)
	char			m_reserved2[8];
	char			m_achPath[256];	// file path, limited to 235 chars + 0-byte
};

// sample info structure (v2)
struct SampleInfo2
{
	PCMWAVEFORMAT	m_wavHeader;	// WAV header
	unsigned int	m_pWavData_ptr32_internal;	// raw sample data
	DWORD			m_dwStart;		// sample start
	DWORD			m_dwEnd;		// sample end
	DWORD			m_dwLoopStart;
	DWORD			m_dwLoopEnd;
	BYTE			m_bLoopSwitch;
	BYTE			m_bRootKey;		// root key (used by AKP)
	BYTE			m_bDontDelete;	// dont delete right half of deinterlaced sample (used by AKP)
	BYTE			m_bModified;	// modified flag
	// more stuff, mainly for audio tracks but also used by the sampler
	char			m_achTitle[20];	// title, limited to 19 chars + 0-byte
	
	// jouni -- these are runtime fields, there might be issue with 64-bit file here
	unsigned int	m_pcsFilePath_ptr32;	// full path name if present, otherwise 0
	unsigned int	m_pPeakData_ptr32;	// peak file data if present, otherwise 0
	CTime			m_ctLastWrite;	// last write access
	BYTE			m_bRes[4];		// reserved
#ifndef _WIN64
	DWORD			m_dwReserved[6];// reserved for future use - need to be zero'ed out
#else
	unsigned long long int m_pWavData_ptr64_internal; // raw sample data (64-bit pointer)
#endif
};

// FXContainer.h

#define NUM_CONTAINER_FX	4

// FxDualStereoSplit.h

#define NUM_DUALSTEREOSPLIT_FX		4

// FxBandSplit.h

#define NUM_BANDSPLIT_FX	6

// DXConnect.h

typedef struct tagDXFXHeader
{
	DWORD	dwFlags;				// header flags
	char	achName[40];			// name of plugin that appears in window title
	int		iChans;					// mono=1, stereo=2
	int	bAssigned;				// assigned somewhere = TRUE
	int		iFxType;				// SEND or INSERT
	CLSID	dxClsID;				// class ID for DX plugins
} DXFXHeader;

// Ultran.h

// waveform order data (16 bytes)
typedef struct tagUltranWDat
{
	BYTE		bLoopStart;
	BYTE		bLoopEnd;
	BYTE		bReserved;
	BYTE		bVersion;
	BYTE		bOrderInx[12];
} UltranWDat;

#define ULTRAN_FLAGS_BUNDLE		1

typedef struct tagUltranHdr
{
	DWORD		dwFlags;		// 1=BUNDLE
	int			iPrgType;		// PROGRAMTYPE_WAV, ...
	int			iSelProg;		// selected program
	DWORD		dwBundleSize;	// data size of attached file
	UltranWDat	wavedat;
} UltranSaveHdr;

typedef struct tagUltranPresetExtHdr
{
	int			iSelProgram;
	UltranWDat	wavedat;
} UltranPresetExtHdr;

// 909Module.h

#define XR909_CHANS			11			// # of channels

#define XR909_SMP_USER_1			4
#define XR909_SMP_USER_2			5

// sample header for user samples
typedef struct tagSmpHeader
{
	DWORD	dwSmpNum;		// sample number (currently 0 or 1)
	DWORD	dwFormat;		// reserved, must be 0
//	char	m_achName[248];
} SmpHeader;
