mod synapse;

use clap::{App};
//use env_logger::Env;

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

fn main() {
    let matches = App::new("orionology")
        .version(VERSION)
        .author("Jouni Airaksinen <jouni@kettureaktio.fi>")
        .about("Reads Synapse Audio SFS (Sonic Factory Song) song file format and outputs information from it.")
        .arg("<input>              'Orion song for file to try to parse'")
        .arg("-v..                 'Sets the level of verbosity (v=info, vv=debug, vvv=trace)'")
        .get_matches();

    let log_level = match matches.occurrences_of("v") {
        0 => "info",
        1 => "debug",
        2 => "trace",
        _ => "trace",
    };

    // pretty_env_logger doesn't have the env_logger::Env
    match std::env::var("RUST_LOG") {
        Ok(_) => {}, // NOP
        Err(_) => std::env::set_var("RUST_LOG", log_level)
    }
    //env_logger::from_env(Env::default().default_filter_or(log_level));
    pretty_env_logger::init();

    //println!("Logging level: {}", log_level);

    // You can check the value provided by positional arguments, or option arguments
    if let Some(input) = matches.value_of("input") {
        println!("Song input filename: [{}]", input);
        synapse::load_song(input);
    }
}
