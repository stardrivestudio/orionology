// Include generated bindings in a pretty allowing fashion
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(dead_code)]
//pub mod sfs {
    include!("./bindings.rs");
//}

use std::io::{Error, ErrorKind};
use std::io::prelude::*;
use std::fs;
use std::mem;

use std::path::PathBuf;

use c_fixed_string::CFixedStr;

use log::{info, error, trace, debug};

pub type SFChunk = SF_EVTDWORD; // Alias for readability

static NO_PATTERN_DEBUG: bool = false;
static NO_CHUNK_DEBUG: bool = false;

//use byteorder::{LittleEndian, ReadBytesExt};
//use bytes::{Bytes, BytesMut, Buf, BufMut};

pub fn chunk_id_to_module_name_str(chunk_id: u32) -> &'static str {
    match chunk_id {
        // Native generators
        SF_OBJ_SAMPLER => "Sampler",
        SF_OBJ_WASP => "Wasp XT",
        SF_OBJ_909 => "XR-909",
        SF_OBJ_VST2SYNTH => "Vst2Synth",
        SF_OBJ_303 => "Monobass",
        SF_OBJ_WAVE => "WaveFusion",
        SF_OBJ_AUDIO => "Audiotrack",
        SF_OBJ_DRUMS => "Drumrack",
        SF_OBJ_MIDI => "Midi Out",
        SF_OBJ_TOMKICK => "Tomcat",
        SF_OBJ_HIHATS => "SF_OBJ_HIHATS", // not used
        SF_OBJ_SCREAMER => "Screamer",
        SF_OBJ_PLUCKED => "Plucked String",
        SF_OBJ_AUTOMATION => "Automation Track",
        SF_OBJ_ULTRAN => "Ultran",
        SF_OBJ_REWIRE => "ReWire",
        SF_OBJ_FMSYNTH => "SF_OBJ_FMSYNTH", // not used
        SF_OBJ_GRAND => "Grand Piano", // not used
        SF_OBJ_DXSYNTH => "DXSynth",
        SF_OBJ_P9 => "Pro-9", // Replaces XR-909 in Orion 8
        // Native effects
        // TODO
        _ => chunk_id_to_str(chunk_id)
    }
}

pub fn chunk_id_to_str(chunk_id: u32) -> &'static str {
    match chunk_id {
        SF_HEADER => "SF_HEADER",
        SF_GLOB => "SF_GLOB",
        SF_EVTS => "SF_EVTS",
        SF_PATC => "SF_PATC",
        SF_WINP => "SF_WINP",
        SF_AUDIO => "SF_AUDIO",
        SF_OBJS => "SF_OBJS",
        SF_CHAN => "SF_CHAN",
        SF_FX_RETURNS => "SF_FX_RETURNS",
        SF_MODULE_END => "SF_MODULE_END",
        SF_FX_SENDS => "SF_FX_SENDS",
        SF_FX_ASSIGNS => "SF_FX_ASSIGNS",
        SF_PAT => "SF_PAT",
        SF_PAT_COUNT => "SF_PAT_COUNT",
        SF_PAT_ORDER => "SF_PAT_ORDER",
        SF_CONTROLS => "SF_CONTROLS",
        SF_CHAN_INFO => "SF_CHAN_INFO",
        SF_PAT_LENGTH => "SF_PAT_LENGTH",
        SF_GEN_INFO => "SF_GEN_INFO",
        SF_FX_INFO => "SF_FX_INFO",
        SF_OBJ_SAMPLER => "SF_OBJ_SAMPLER",
        SF_OBJ_WASP => "SF_OBJ_WASP",
        SF_OBJ_909 => "SF_OBJ_909",
        SF_OBJ_VST2SYNTH => "SF_OBJ_VST2SYNTH",
        SF_OBJ_303 => "SF_OBJ_303",
        SF_OBJ_WAVE => "SF_OBJ_WAVE",
        SF_OBJ_AUDIO => "SF_OBJ_AUDIO",
        SF_OBJ_DRUMS => "SF_OBJ_DRUMS",
        SF_OBJ_MIDI => "SF_OBJ_MIDI",
        SF_OBJ_TOMKICK => "SF_OBJ_TOMKICK",
        SF_OBJ_HIHATS => "SF_OBJ_HIHATS",
        SF_OBJ_SCREAMER => "SF_OBJ_SCREAMER",
        SF_OBJ_PLUCKED => "SF_OBJ_PLUCKED",
        SF_OBJ_AUTOMATION => "SF_OBJ_AUTOMATION",
        SF_OBJ_ULTRAN => "SF_OBJ_ULTRAN",
        SF_OBJ_REWIRE => "SF_OBJ_REWIRE",
        SF_OBJ_FMSYNTH => "SF_OBJ_FMSYNTH",
        SF_OBJ_GRAND => "SF_OBJ_GRAND",
        SF_OBJ_DXSYNTH => "SF_OBJ_DXSYNTH",
        SF_OBJ_P9 => "SF_OBJ_P9",
        //SF_OBJ_LASTGEN => "SF_OBJ_LASTGEN",

        //SF_OBJ_FIRSTFX => "SF_OBJ_FIRSTFX",
        SF_OBJ_DELAY => "SF_OBJ_DELAY",
        SF_OBJ_REVERB => "SF_OBJ_REVERB",
        SF_OBJ_PHASER => "SF_OBJ_PHASER",
        SF_OBJ_DISTORTION => "SF_OBJ_DISTORTION",
        SF_OBJ_VST => "SF_OBJ_VST",
        SF_OBJ_COMPRESSOR => "SF_OBJ_COMPRESSOR",
        SF_OBJ_TRANCEGATE => "SF_OBJ_TRANCEGATE",
        SF_OBJ_DELAY_STEREO => "SF_OBJ_DELAY_STEREO",
        SF_OBJ_CHORUS => "SF_OBJ_CHORUS",
        SF_OBJ_PCF => "SF_OBJ_PCF",
        SF_OBJ_ULTRAVERB => "SF_OBJ_ULTRAVERB",
        SF_OBJ_X_DELAY => "SF_OBJ_X_DELAY",
        SF_OBJ_TS9DIST => "SF_OBJ_TS9DIST",
        SF_OBJ_DX => "SF_OBJ_DX",
        SF_OBJ_FILTER => "SF_OBJ_FILTER",
        SF_OBJ_NUPHASER => "SF_OBJ_NUPHASER",
        SF_OBJ_FLANGER => "SF_OBJ_FLANGER",
        SF_OBJ_FILTERDELAY => "SF_OBJ_FILTERDELAY",
        SF_OBJ_ECHO => "SF_OBJ_ECHO",
        SF_OBJ_ULTRAVERB2 => "SF_OBJ_ULTRAVERB2",
        SF_OBJ_PLATE_REVERB => "SF_OBJ_PLATE_REVERB",
        SF_OBJ_RINGMOD => "SF_OBJ_RINGMOD",
        SF_OBJ_ANALYZER => "SF_OBJ_ANALYZER",
        SF_OBJ_TALKBOX => "SF_OBJ_TALKBOX",
        SF_OBJ_GATE => "SF_OBJ_GATE",
        SF_OBJ_CLIPPER => "SF_OBJ_CLIPPER",
        SF_OBJ_VOCODER => "SF_OBJ_VOCODER",
        SF_OBJ_PARAM_EQ => "SF_OBJ_PARAM_EQ",
        SF_OBJ_LIMITER => "SF_OBJ_LIMITER",
        SF_OBJ_LATENCY => "SF_OBJ_LATENCY",
        SF_OBJ_CHORUS2 => "SF_OBJ_CHORUS2",
        SF_OBJ_STEREOWIDEN => "SF_OBJ_STEREOWIDEN",
        SF_OBJ_PLAT_ECHO => "SF_OBJ_PLAT_ECHO",
        SF_OBJ_TREMOLO => "SF_OBJ_TREMOLO",
        SF_OBJ_LESLIE => "SF_OBJ_LESLIE",
        SF_OBJ_VOLUME => "SF_OBJ_VOLUME",
        SF_OBJ_HARM_FILTER => "SF_OBJ_HARM_FILTER",
        SF_OBJ_FILTER_24 => "SF_OBJ_FILTER_24",
        SF_OBJ_BAND_SPLIT => "SF_OBJ_BAND_SPLIT",
        SF_OBJ_HARM_DELAY => "SF_OBJ_HARM_DELAY",
        SF_OBJ_REVERB3 => "SF_OBJ_REVERB3",
        SF_OBJ_FXCONTAINER => "SF_OBJ_FXCONTAINER",
        SF_OBJ_LOFI => "SF_OBJ_LOFI",
        SF_OBJ_COMP_MULTI => "SF_OBJ_COMP_MULTI",
        SF_OBJ_SSBMOD => "SF_OBJ_SSBMOD",
        SF_OBJ_FFT_1 => "SF_OBJ_FFT_1",
        SF_OBJ_FFT_2 => "SF_OBJ_FFT_2",
        SF_OBJ_FFT_3 => "SF_OBJ_FFT_3",
        SF_OBJ_FFT_FORMANT => "SF_OBJ_FFT_FORMANT",
        SF_OBJ_LOWCUT => "SF_OBJ_LOWCUT",
        SF_OBJ_ANALOG_EQ => "SF_OBJ_ANALOG_EQ",
        SF_OBJ_DIFFDELAY1 => "SF_OBJ_DIFFDELAY1",
        SF_OBJ_DIFFDELAY2 => "SF_OBJ_DIFFDELAY2",
        SF_OBJ_HARD_LIMITER => "SF_OBJ_HARD_LIMITER",
        SF_OBJ_NDISTORTION => "SF_OBJ_NDISTORTION",
        SF_OBJ_FREEDYNAMICS => "SF_OBJ_FREEDYNAMICS",
        SF_OBJ_NUGATE => "SF_OBJ_NUGATE",
        SF_OBJ_NULIMITER => "SF_OBJ_NULIMITER",
        SF_OBJ_NUCOMPRESSOR => "SF_OBJ_NUCOMPRESSOR",
        SF_OBJ_TREMOLO2 => "SF_OBJ_TREMOLO2",
        SF_OBJ_SOMEFX => "SF_OBJ_SOMEFX",
        SF_OBJ_SD1_WRAPPER => "SF_OBJ_SD1_WRAPPER",
        SF_OBJ_NUPANNER => "SF_OBJ_NUPANNER",
        SF_OBJ_EQ_TRACKING1 => "SF_OBJ_EQ_TRACKING1",
        SF_OBJ_EQ_TRACKING2 => "SF_OBJ_EQ_TRACKING2",
        SF_OBJ_EQ_TRACKING4 => "SF_OBJ_EQ_TRACKING4",
        SF_OBJ_EQ_TRACKING6 => "SF_OBJ_EQ_TRACKING6",
        SF_OBJ_PEAKLIMIT => "SF_OBJ_PEAKLIMIT",
        SF_OBJ_VINTAGE_EQ => "SF_OBJ_VINTAGE_EQ",
        SF_OBJ_HIGHCUT => "SF_OBJ_HIGHCUT",
        SF_OBJ_NUCHORUS => "SF_OBJ_NUCHORUS",
        SF_OBJ_NUPHASER2 => "SF_OBJ_NUPHASER2",
        SF_OBJ_DUALSSPLIT => "SF_OBJ_DUALSSPLIT",
        SF_OBJ_FILTER_SALLEN_KEY => "SF_OBJ_FILTER_SALLEN_KEY",
        // Orion 8 -->
        SF_OBJ_2TAP_DELAY => "SF_OBJ_2TAP_DELAY",
        SF_OBJ_3TAP_DELAY => "SF_OBJ_3TAP_DELAY",
        SF_OBJ_OCTAVER => "SF_OBJ_OCTAVER",
        SF_OBJ_PITCHSHIFT => "SF_OBJ_PITCHSHIFT",
        SF_OBJ_AMPCABINET => "SF_OBJ_AMPCABINET",	
        SF_OBJ_NUDSTRTION => "SF_OBJ_NUDSTRTION",
        SF_OBJ_NUPHASER3 => "SF_OBJ_NUPHASER3",	
        SF_OBJ_NUCHORUS2 => "SF_OBJ_NUCHORUS2",	
        SF_OBJ_BRICKWALL => "SF_OBJ_BRICKWALL",	
        SF_OBJ_MULTIBLIM => "SF_OBJ_MULTIBLIM",	
        SF_OBJ_X_DELAYPRO => "SF_OBJ_X_DELAYPRO",	
        SF_OBJ_DELAY2 => "SF_OBJ_DELAY2",
        SF_OBJ_DELAY3 => "SF_OBJ_DELAY3",		
        SF_OBJ_REVERB_R1 => "SF_OBJ_REVERB_R1",
        SF_OBJ_REVERB_R2 => "SF_OBJ_REVERB_R2",	
        SF_OBJ_RMM2 => "SF_OBJ_RMM2",
        
        SF_OBJ_FXTEST => "SF_OBJ_FXTEST",
        //SF_OBJ_LASTFX => "SF_OBJ_LASTFX",
        SF_OBJ_NAME => "SF_OBJ_NAME",
        SF_OBJ_END => "SF_OBJ_END",
        SF_EOF => "SF_EOF",
        _ => "unknown"
    }
}

pub fn subchunk_id_to_str(chunk_id: u32) -> &'static str {
    match chunk_id {
        SF_SMP_POOL => "SF_SMP_POOL",
        SF_SMP_HEADER => "SF_SMP_HEADER",
        SF_SMP_DATA => "SF_SMP_DATA",
        SF_PROGRAM => "SF_PROGRAM",
        SF_PROGRAM_ID => "SF_PROGRAM_ID",
        SF_TRACK_DATA => "SF_TRACK_DATA",
        SF_GEN_DATA => "SF_GEN_DATA",
        //SF_WT_INFO => "SF_WT_INFO",
        _ => event_id_to_str(chunk_id)
    }
}

pub fn event_id_to_str(event_id: u32) -> &'static str {
    match event_id {
        EVENT_SONGINFO => "EVENT_SONGINFO",
        _ => "unknown"
    }
}

pub fn program_type_to_str(program_type: u32) -> &'static str {
    match program_type {
        PROGRAMTYPE_SF2 => "PROGRAMTYPE_SF2",
        PROGRAMTYPE_KRZ => "PROGRAMTYPE_KRZ",
        PROGRAMTYPE_WAV => "PROGRAMTYPE_WAV",
        PROGRAMTYPE_ORION => "PROGRAMTYPE_ORION",
        PROGRAMTYPE_AKAI => "PROGRAMTYPE_AKAI",
        PROGRAMTYPE_TXT => "PROGRAMTYPE_TXT",
        _ => "unknown"
        //_ => format!("unknown/{}", program_type)
    }
}

pub fn get_module_type(module_id: u32) -> &'static str {
    if module_id <= SF_OBJ_LASTGEN {
        "Generator"
    } else {
        "Effect"
    }
}

pub fn xr909_channel_to_str(channel_index: u32) -> &'static str {
    match channel_index {
        0 => "Bassdrum", 
        1 => "Snare", 
        2 => "Clap", 
        3 => "Rimshot", 
        4 => "Closed Hh", 
        5 => "Open Hh", 
        6 => "Ride Cym", 
        7 => "Crash Cym", 
        8 => "User-1", 
        9 => "User-2",
        _ => "unknown"
    }
}

// Special magic value (fixed separator chunk) added in later Orion versions to fix broken VST plugin loading
// Technically this can be used to identify actual module start/end from file data and delete or fix the chunk size
// if it has become mis-matched. I recall adding this one to the Orion to support easier scanning and recovery of Orion file structure
// with my old SFSAnalyzer tool. The main chunk's store their sizes as 0, so it's harder to detect end of module
// if you don't know the full data structure of the specific following module.
const END_ORION_MODULE_MAGIC: &'static [u8; 22] = b"OMOD\x10\x00\x00\x00EndOrionModule";

// TODO: refactor funcs to be impl of this struct to fill
#[derive(Debug, Default)] 
pub struct OrionSong {
    filename: String,
    version: u32,
}

pub fn load_song(filename: &str) {
    let mut song: OrionSong = OrionSong::default();

    load_song_data(filename, &mut song).expect("Error loading song file");

    println!("File name: {}", song.filename);
    println!("File version: {:x}", song.version);

    println!("All good. Time to make some music?");
}

// Internal funcs for loading the struct data
fn read_fixed_struct_unsafe<T>(f: &mut dyn Read, size: u64, data_size: u64) -> std::io::Result<T>
    where T: Copy,
    T: std::fmt::Debug {
    let mut buf = Vec::new();
    trace!("read_struct_unsafe:size={}, data_size={}", size, data_size);

    f.take(data_size).read_to_end(&mut buf)?;

    // Pad to avoid "undefined behaviour" if buf is smaller than struct_data

    // In our context, this may happen if reading old chunk data (old song file) and our structs are later version
    // with new fields at the end. Readers will modify defaults if zeros are not good enough. This matches Orion's 
    // c++ way of reading structs in.
    trace!("read_struct_unsafe: buf.len()={}, size={}", buf.len(), size);
    let size_diff = size as usize - buf.len();
    if size_diff > 0 {
        trace!("read_struct_unsafe:size_diff={} ; needs padding", size_diff);
        buf.append(&mut vec![0u8; size_diff]);
    }

    let struct_data: &T = unsafe { mem::transmute_copy(&buf) };

    trace!("read_struct_unsafe:struct_data={:?}", *struct_data);

    Ok(*struct_data)
}

fn read_sized_struct_unsafe<T>(f: &mut dyn Read, data_size: u32) -> std::io::Result<T> 
    where T: Copy,
    T: std::fmt::Debug {
    let size = mem::size_of::<T>() as u64;
    read_fixed_struct_unsafe::<T>(f, size, data_size as u64)
}

fn read_struct_unsafe<T>(f: &mut dyn Read) -> std::io::Result<T> 
    where T: Copy,
    T: std::fmt::Debug {
    let size = mem::size_of::<T>() as u64;
    // We expect read bytes and struct size to match
    read_fixed_struct_unsafe::<T>(f, size, size)
}

// Chunk loading funcs
fn skip_chunk(f: &mut dyn Read, byte_count: u32) -> std::io::Result<Vec<u8>> {
    // Fairly naive way of progressing the loader pointer (cursor/seek maybe?)
    let mut buf = Vec::new();
    if byte_count > 0 {    
        trace!("skipping data length={:#x}", byte_count);
        f.take(byte_count as u64).read_to_end(&mut buf)?;
    }
    Ok(buf)
}

pub fn print_chunk_debug(evt: &SFChunk) {
    if !NO_CHUNK_DEBUG {
        debug!("CHUNK {}:{:x}, size={:#x}", chunk_id_to_str(evt.id), evt.id, evt.data);
    }
}

pub fn print_subchunk_debug(evt: &SFChunk) {
    if !NO_CHUNK_DEBUG {
        debug!("** SUB-CHUNK {}:{:x}, size={:#x}", subchunk_id_to_str(evt.id), evt.id, evt.data);
    }
}

// Represents expected chunk_id
pub enum SFChunkId {
    Id(u32),
    Any()
}

// Chunk event is the header proceeding 
pub fn read_chunk_event(f: &mut dyn Read, expect_chunk_id: SFChunkId) -> std::io::Result<SFChunk> {
    let evt = read_struct_unsafe::<SFChunk>(f)?;
    if let SFChunkId::Id(chunk_id) = expect_chunk_id {
        assert!(evt.id == chunk_id, "oh-no, not {}:{} == {}:{}", chunk_id_to_str(evt.id), evt.id, chunk_id_to_str(chunk_id), chunk_id);
    }
    print_chunk_debug(&evt);
    Ok(evt)
}

pub fn read_subchunk_event(f: &mut dyn Read, expect_chunk_id: SFChunkId) -> std::io::Result<SFChunk> {
    let evt = read_struct_unsafe::<SFChunk>(f)?;
    if let SFChunkId::Id(chunk_id) = expect_chunk_id {
        assert!(evt.id == chunk_id, "oh-no, not {}:{} == {}:{}", subchunk_id_to_str(evt.id), evt.id, subchunk_id_to_str(chunk_id), chunk_id);
    }
    print_subchunk_debug(&evt);
    Ok(evt)
}

pub fn read_sized_chunk_data<T>(f: &mut dyn Read, evt: SFChunk) -> std::io::Result<T> 
    where T: Copy,
    T: std::fmt::Debug {
    // TODO: combine with read_chunk/read_subchunk_event
    // unneeded data copy here.. fix later
    let struct_data = read_sized_struct_unsafe::<T>(f, evt.data)?;
    Ok(struct_data)
}

pub fn read_chunk_data<T>(f: &mut dyn Read) -> std::io::Result<T> 
    where T: Copy,
    T: std::fmt::Debug {
    // TODO: combine with read_chunk/read_subchunk_event
    // unneeded data copy here.. fix later
    let struct_data = read_struct_unsafe::<T>(f)?;
    Ok(struct_data)
}

fn read_dynamic_chunk_data(f: &mut dyn Read, data_size: u32) -> std::io::Result<Vec<u8>> {
    let mut buf: Vec<u8> = Vec::new();
    trace!("read_dynamic_chunk_data:data_size={}", data_size);

    f.take(data_size as u64).read_to_end(&mut buf)?;
    Ok(buf)
}

// Read single u32 data value
fn read_u32(f: &mut dyn Read) -> std::io::Result<u32> {
    let mut buf = [0u8; 4];
    f.read_exact(&mut buf)?;
    Ok(u32::from_le_bytes(buf))
}

// Read single u8 data value
fn read_u8(f: &mut dyn Read) -> std::io::Result<u8> {
    let mut buf = [0u8; 1];
    f.read_exact(&mut buf)?;
    Ok(u8::from_le_bytes(buf))
}

// Read single i32 data value
fn read_i32(f: &mut dyn Read) -> std::io::Result<i32> {
    let mut buf = [0u8; 4];
    f.read_exact(&mut buf)?;
    Ok(i32::from_le_bytes(buf))
}

// Conversion funcs
fn u8_as_bool(b: u8) -> bool {
    if b == 0 {
        return false
    } else {
        return true
    }
}

fn bool_to_str(b: bool) -> &'static str {
    match b {
        true => "ON",
        false => "OFF"
    }
}

fn bit_flag(value: u32, flags: u32) -> bool {
    value & flags == flags
}

fn fixed_c_string(cstr: &[::std::os::raw::c_char]) -> &str {
    unsafe {
            match CFixedStr::from_ptr(cstr.as_ptr() as *const i8, cstr.len()).to_str() {
            Ok(s) => s,
            Err(_) => "-"
        }
    }
}

fn read_utf8_string(f: &mut dyn Read, bytes_count: u32) -> std::io::Result<String> {
    if bytes_count == 0 {
        return Ok("".to_string());
    }
    let data = read_dynamic_chunk_data(f, bytes_count)?;
    // Assuming utf-8 encoding for simplicity as we have non nul terminated string
    Ok(match String::from_utf8(data) {
        Ok(cstr) => cstr,
        Err(_) => "".to_string()
    })
}

pub fn module_index_to_str(module_index: i32) -> String {
    // TODO: module_index should really be Option<u32>
    match module_index {
        -1 => "----".to_string(),
        _ => format!("Module #{}", module_index)
    }
}

static PATTERN_PREFIX: [&str; 8] = ["A", "B", "C", "D", "E", "F", "G", "H"];

// Pretty print of generated pattern name in form as displayed in Orion; A1, A2, A3 ...
pub fn pattern_name(pattern_index: Option<i32>) -> String {
    match pattern_index {
        Some(index) => format!("{}{}", PATTERN_PREFIX[(index as usize) % 8], index / 8 + 1),
        None => "".to_string()
    }
}

// Read Generator header chunk
pub fn read_gen_native_header_chunk(f: &mut dyn Read, chunk_id: u32, current_module_id: i32) -> std::io::Result<GenHeader> {
    let evt = read_struct_unsafe::<SFChunk>(f)?;
    assert!(evt.id == SF_GEN_INFO, "oh-no, not SF_GEN_INFO {:?}", evt);

    Ok(read_gen_native_header(f, &evt, chunk_id, current_module_id)?)
}

pub fn read_gen_native_header(f: &mut dyn Read, evt: &SFChunk, chunk_id: u32, current_module_id: i32) -> std::io::Result<GenHeader> {
    // GenHeader size and evt.data may not match in size between file version differences
    let gen_header = read_sized_struct_unsafe::<GenHeader>(f, evt.data)?;
    info!("Generator/{} #{}: [{}]", chunk_id_to_module_name_str(chunk_id), current_module_id, fixed_c_string(&gen_header.achName));
    info!("** Version: {}", gen_header.bGenVersion);
    info!("** Current preset: [{}]", fixed_c_string(&gen_header.achPresetName));
    info!("** Mixer channel count: [{}]", gen_header.bChannelCount);
    debug!("** GenHeader {:?}", gen_header);
    Ok(gen_header)
}

// Read VST2Synth header
pub fn read_gen_vst2synth_header(f: &mut dyn Read, chunk_id: u32, current_module_id: i32, file_version: u32) -> std::io::Result<Vst2Header> {
    let evt = read_struct_unsafe::<SFChunk>(f)?;
    assert!(evt.id == SF_GEN_INFO, "oh-no, not SF_GEN_INFO {:?}", evt);

    // GenHeader size and evt.data may not match in size between file version differences
    let mut gen_header = read_sized_struct_unsafe::<Vst2Header>(f, evt.data)?;
    if file_version <= 0x00002600 {
		// some data missing in old VST chunks, initialize it
		gen_header.nNumChannels = 2;
        gen_header.bIsStereoChan[0] = 1;
    }
    let plugin_path = read_utf8_string(f, gen_header.nPathLen as u32)?;

    info!("Generator/{} #{}: [{}]", chunk_id_to_module_name_str(chunk_id), current_module_id, fixed_c_string(&gen_header.achName));
    info!("** Plugin name: [{}]", plugin_path);
    info!("** Current program number: [{}]", gen_header.lNumProgram);
    info!("** Mixer channel count: [{}]", gen_header.nNumChannels);
    debug!("** Vst2Header {:?}", gen_header);

    Ok(gen_header)
}

pub fn read_gen_vst2synth(f: &mut dyn Read, gen_header: &Vst2Header, current_module_id: i32, _file_version: u32) -> std::io::Result<()> {
    let evt = read_struct_unsafe::<SFChunk>(f)?;

    // Normally Orion uses support state read from the vst instance, luckily it's stored as flag...
    let vst_supports_chunks = bit_flag(gen_header.dwFlags, GENFLAGS_VST2CHUNK);

    let controls;
    if evt.id == SF_CONTROLS {
        // Old versions have this first
        controls = read_controls(f, &evt)?;
    } else {
        // TODO: recfactor; not optimum reuse of function..
        let header = read_gen_native_header(f, &evt, SF_OBJ_VST2SYNTH, current_module_id)?;
        controls = read_controls_chunk(f)?;

        if !vst_supports_chunks {
            if bit_flag(header.dwFlags, GENFLAGS_VST2COMPLETEBANK) {
                // VST was stored with "full banks save" enabled in Orion (and no chunk support in VST)
                //
                // TODO: right now utterly fails in this kind of songs
                //
                // Unfortunately Orion uses numPrograms read from running VST instance and does not save this into headers.
                // Only way to load would be load in pairs of  "CtlData * control_count" + "32-byte program name" and 
                // see that if there's SF_GEN_DATA event-chunk coming next before loading next program (and hope the CtlData 
                // does not match the SF_GEN_DATA chunk id).
                //
                // TODO: Orion 8: added gen_header.lPrograms for this.
            }
        }
    }
    if vst_supports_chunks {
        // Basically this is more or less the currently loaded fxp/fxb (preset) core data
        let vst_chunk_evt = read_subchunk_event(f, SFChunkId::Id(SF_GEN_DATA))?;
        skip_chunk(f, vst_chunk_evt.data)?; // NOP
        info!("** Vst chunk data size: [{} bytes]", vst_chunk_evt.data);
    } else {
        info!("** No Vst chunk; control data used: [{}]", controls.len());
    }
    Ok(())
}

// Read VST2Effect header
pub fn read_gen_vst2effect_header(f: &mut dyn Read, chunk_id: u32, current_module_id: i32, file_version: u32) -> std::io::Result<VstFxHeader> {
    let evt = read_struct_unsafe::<SFChunk>(f)?;
    assert!(evt.id == SF_FX_INFO, "oh-no, not SF_FX_INFO {:?}", evt);

    // VstFxHeader size and evt.data may not match in size between file version differences
    let fx_header = read_sized_struct_unsafe::<VstFxHeader>(f, evt.data)?;

    let plugin_path;
    if file_version >= 0x1350 {
        plugin_path = read_utf8_string(f, fx_header.nPathLen as u32)?;
    } else {
        // Old loader
        // NOP
        plugin_path = fixed_c_string(&fx_header.achName).to_string();
    }

    info!("Effect/{} #{}: [{}]", chunk_id_to_module_name_str(chunk_id), current_module_id, fixed_c_string(&fx_header.achName));
    info!("** Plugin name: [{}]", plugin_path);
    info!("** Current program number: [{}]", fx_header.lNumProgram);
    debug!("** VstFxHeader {:?}", fx_header);

    Ok(fx_header)
}


pub fn read_gen_vst2effect(f: &mut dyn Read, fx_header: &VstFxHeader, _file_version: u32) -> std::io::Result<()> {
    let controls = read_controls_chunk(f)?;

    let vst_supports_chunks = u8_as_bool(fx_header.bBundle);

    if vst_supports_chunks {
        // Basically this is more or less the currently loaded fxp/fxb (preset) core data
        let vst_chunk_evt = read_subchunk_event(f, SFChunkId::Id(SF_GEN_DATA))?;
        skip_chunk(f, vst_chunk_evt.data)?; // NOP
        info!("** Vst chunk data size: [{}]", vst_chunk_evt.data);
    } else {
        // Orion would now update the read_controls data to the VST instance
        // NOP
        info!("** No Vst chunk; control data used: [{}]", controls.len());
    }
    Ok(())
}

pub fn read_native_effect_header(f: &mut dyn Read, chunk_id: u32, current_module_id: i32, file_version: u32) -> std::io::Result<FXHeader> {
    let evt = read_struct_unsafe::<SFChunk>(f)?;
    assert!(evt.id == SF_FX_INFO, "oh-no, not SF_FX_INFO {:?}", evt);

    let fx_header = read_sized_struct_unsafe::<FXHeader>(f, evt.data)?;

    if file_version <= 0x00002600 {
        // TODO: remap sidechain values
    }

    info!("Effect/{} #{}: [{}]", chunk_id_to_module_name_str(chunk_id), current_module_id, fixed_c_string(&fx_header.achName));
    //info!("** Current program number: [{}]", fx_header.lNumProgram);
    debug!("** FXHeader {:?}", fx_header);

    let controls = read_controls_chunk(f)?;

    // For informative purposes embedded here, better would be to refactor code to read specific chunk structs with output code
    // Here because of need for controls data.
    if chunk_id == SF_OBJ_FXCONTAINER && controls.len() >= NUM_CONTAINER_FX as usize {
        info!("** MultiFX Control:");
        // control mapping is static in Orion code
        for fx_index in 0..NUM_CONTAINER_FX {
            info!("**** Fx{}: [{}] [{}]", 
                fx_index + 1, 
                module_index_to_str(controls[fx_index as usize].iCtrlValue), 
                bool_to_str(controls[(fx_index + NUM_CONTAINER_FX) as usize].iCtrlValue > 0)
            );
        }
    }
    if chunk_id == SF_OBJ_DUALSSPLIT && controls.len() >= NUM_DUALSTEREOSPLIT_FX as usize {
        info!("** StereoFX Control:");
        // control mapping is static in Orion code
        for fx_index in 0..NUM_DUALSTEREOSPLIT_FX {
            info!("**** Fx{}: [{}]", 
                fx_index + 1, 
                module_index_to_str(controls[fx_index as usize].iCtrlValue)
            );
        }
    }
    if chunk_id == SF_OBJ_BAND_SPLIT && controls.len() >= NUM_BANDSPLIT_FX as usize {
        info!("** BandFX Control:");
        // control mapping is static in Orion code
        for fx_index in 0..NUM_BANDSPLIT_FX {
            info!("**** Fx{}: [{}]", 
                fx_index + 1, 
                module_index_to_str(controls[fx_index as usize].iCtrlValue)
            );
        }
        // TODO: Band 1+2+3 ON/OFF
    }
    
    Ok(fx_header)
}

pub fn read_native_effect_fxcontainer(_f: &mut dyn Read, _fx_header: &FXHeader, _file_version: u32) -> std::io::Result<()> {
    // nop
    Ok(())
}

pub fn read_native_effect_pcf(f: &mut dyn Read, _fx_header: &FXHeader, file_version: u32) -> std::io::Result<()> {
    // PCF has patterns
    read_patterns(f, file_version)?;
    // nop
    Ok(())
}

pub fn read_native_effect_fft_3(f: &mut dyn Read, _fx_header: &FXHeader, _file_version: u32) -> std::io::Result<()> {

    let ir_filename_length = read_u32(f)?;
    let ir_filename = read_utf8_string(f, ir_filename_length)?;
    info!("** Impulse response filename: [{}]", ir_filename);

    // nop
    Ok(())
}

pub fn read_dx_effect_header(f: &mut dyn Read, chunk_id: u32, current_module_id: i32, _file_version: u32) -> std::io::Result<()> {
    let evt = read_struct_unsafe::<SFChunk>(f)?;
    assert!(evt.id == SF_FX_INFO, "oh-no, not SF_FX_INFO {:?}", evt);

    let fx_header = read_sized_struct_unsafe::<DXFXHeader>(f, evt.data)?;

    let dx_chunk_evt = read_chunk_event(f, SFChunkId::Id(SF_CONTROLS))?;
    skip_chunk(f, dx_chunk_evt.data)?;

    info!("Effect/{} #{}: [{}]", chunk_id_to_module_name_str(chunk_id), current_module_id, fixed_c_string(&fx_header.achName));  
    debug!("** DXFXHeader {:?}", fx_header);
    info!("** Plugin name: [{}]", fixed_c_string(&fx_header.achName));
    info!("** Plugin class: [{}]", 
        format!("{{{:08x}-{:04x}-{:04x}-{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}}}", 
            fx_header.dxClsID.Data1, fx_header.dxClsID.Data2, fx_header.dxClsID.Data3,
            fx_header.dxClsID.Data4[0], fx_header.dxClsID.Data4[1], fx_header.dxClsID.Data4[2], fx_header.dxClsID.Data4[3],
            fx_header.dxClsID.Data4[4], fx_header.dxClsID.Data4[5], fx_header.dxClsID.Data4[6], fx_header.dxClsID.Data4[7])
    );
    info!("** Dx chunk data size: [{}]", dx_chunk_evt.data);
    Ok(())
}

pub fn read_dx_synth_header(f: &mut dyn Read, chunk_id: u32, current_module_id: i32, file_version: u32) -> std::io::Result<()> {
    let evt = read_struct_unsafe::<SFChunk>(f)?;
    assert!(evt.id == SF_FX_INFO, "oh-no, not SF_FX_INFO {:?}", evt);

    let fx_header = read_sized_struct_unsafe::<DXFXHeader>(f, evt.data)?;

    read_patterns(f, file_version)?;

    let _gen_header = read_gen_native_header_chunk(f, chunk_id, current_module_id)?;

    let dx_chunk_evt = read_chunk_event(f, SFChunkId::Id(SF_CONTROLS))?;
    skip_chunk(f, dx_chunk_evt.data)?;

    //info!("Generator/{} #{}: [{}]", chunk_id_to_module_name_str(chunk_id), current_module_id, fixed_c_string(&fx_header.achName));  
    debug!("** DXFXHeader {:?}", fx_header);
    info!("** Plugin name: [{}]", fixed_c_string(&fx_header.achName));
    info!("** Plugin class: [{}]", 
        format!("{{{:08x}-{:04x}-{:04x}-{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}}}", 
            fx_header.dxClsID.Data1, fx_header.dxClsID.Data2, fx_header.dxClsID.Data3,
            fx_header.dxClsID.Data4[0], fx_header.dxClsID.Data4[1], fx_header.dxClsID.Data4[2], fx_header.dxClsID.Data4[3],
            fx_header.dxClsID.Data4[4], fx_header.dxClsID.Data4[5], fx_header.dxClsID.Data4[6], fx_header.dxClsID.Data4[7])
    );
    info!("** Dx chunk data size: [{}]", dx_chunk_evt.data);
    Ok(())
}

pub fn read_window_positions(f: &mut dyn Read, window_count: u32) -> std::io::Result<()>  {
    // Just skip window data, we don't really need it
    skip_chunk(f, window_count * mem::size_of::<WindowPosInfo>() as u32)?;
    Ok(())
}

pub fn read_generator_tracks(f: &mut dyn Read, flags: u32) -> std::io::Result<()>  {
    if bit_flag(flags, GENFLAGS_PLAYLISTTRACKS) {
        debug!("** GENFLAGS_PLAYLISTTRACKS set [{:#x} & {:#x}]", flags, GENFLAGS_PLAYLISTTRACKS);

        let evt2 = read_subchunk_event(f, SFChunkId::Id(SF_TRACK_DATA))?;
        let playlist_track = read_sized_chunk_data::<PlaylistTrack>(f, evt2)?;

        debug!("** PlaylistTrack={:?}", playlist_track);
        info!("** Playlist track: [Color: {:#08x}] [MUTE: {} | SOLO: {}]", 
            playlist_track.color /*RGB*/,
            bool_to_str(u8_as_bool(playlist_track.bMuteOn)),
            bool_to_str(u8_as_bool(playlist_track.bSoloOn)));
    };
    Ok(())
}

// Read pattern data chunk(s)
pub fn read_patterns(f: &mut dyn Read, file_version: u32) -> std::io::Result<()> {
    read_chunk_event(f, SFChunkId::Id(SF_PAT_COUNT))?;
    let pattern_count = read_u32(f)?;
    info!("** Pattern count: {}", pattern_count);

    // TODO: temporary holder of data for now, we are not collecting these besides displaying info!
    #[derive(Debug, Default)] 
    struct Pattern {
        index: Option<i32>,
        length: u32,
        event_count: Option<u32>,
        name: Option<String>
    };

    let mut patterns: Vec<Pattern> = Vec::with_capacity(::std::cmp::max(pattern_count, 64) as usize);

    let mut pattern: Pattern = Pattern::default();    
    let mut pattern_index: i32 = 0;
    let mut count = pattern_count;
    loop {
        let evt = read_chunk_event(f, SFChunkId::Any())?;

        match evt.id {
            SF_PAT_LENGTH => {
                // We use SF_PAT_LENGTH as indicator for "next pattern data follows"
                if pattern.index != None {
                    patterns.push(pattern);
                    pattern = Pattern::default();     
                }

                pattern.index = Some(pattern_index);
                pattern_index += 1;

                // Length is in internal ticks (minimum resolution)
                // Quick documention:
                //
                //  PPQ is configurable, default is 96 (96/4=24)
                //  basically PPQ/4 = Resolution, Length = Resolution * SubSteps
                //  SubSteps is pattern length in steps, default is 16.
                //  StepsPerBar is configurable, default is 16.
                //  StepsPerBar = 1 Bar
                //
                //  For example at 96PPQ: (96/4)*16 = 384 ticks
                //
                // I am not sure why it's divided by 4. Probably for performance. You can change PPQ higher, see
                // value from GlobCfg.m_dwTicksPerBar == PPQ/4 * StepsPerBar
                // .
                pattern.length = read_u32(f)?;
                assert!(pattern.length > 0, "Pattern length 0, corrupt pattern? [{:?}]", pattern.index);
            },
            SF_PAT => {
                // Pattern data load starts here
                count -= 1;

                // Since file version >= 0x6500 (magic) there can be two SF_PAT chunks where first 
                // contains pattern events and second pattern name. These *should* come in pairs.
                // Unfortunately it is using the same chunk id and there is no way of detecting which is which (they both could be 32-bytes long)
                // Our loader now works so that it reads one SF_PAT as events and other as pattern name.

                // Orion does the loading in static ordering of the chunks. Our loader works same way by assuming the SF_PAT pair
                // when/if it's encountered. Would need chunk lookup to know when all patterns are loaded. 

                let event_count: u32;
                // TODO: easier to ref file_version if we'd have impl of OrionSong here. Now we are going to panic on too old songs
                // should be fixed with some refactoring. There exists two versions of event structure. Orion does the conversions
                // on the fly when loading older files. Count of events is determined by splitting the chunk data into array of struct.

                if file_version >= 0x2300 {
                    event_count = evt.data / mem::size_of::<Event>() as u32;
                } else {
                    event_count = evt.data / mem::size_of::<OldEvent>() as u32;
                }
                pattern.event_count = Some(event_count);
                if event_count > 0 {
                    skip_chunk(f, evt.data)?; // NOP (TODO: for now we don't load events anywhere)
                    // TODO: OldEvent conversion
                }

                // We have already loaded the events for this pattern, so this "must" be the pattern name
                if file_version >= 0x6500 {
                    let evt = read_chunk_event(f, SFChunkId::Any())?;
                    if pattern.name == None {
                        pattern.name = Some(read_utf8_string(f, evt.data)?);
                    } 
                }
            },
            _ => {
                // NOP
                debug!("Unhandled chunk encountered while loading patterns {:?}", evt);
                skip_chunk(f, evt.data)?;
            }
        }
        // Check if we still would have more patterns
        // TODO: Think better way of detecting this than relying on the given count
        if count <= 0 {
            trace!("read_patterns:done, count={}", count);
            break;
        }
    }
    // Last lingering pattern in our loop
    if pattern.index != None {
        patterns.push(pattern)
    }

    // Debug output of the results
    if !NO_PATTERN_DEBUG {
        for pattern in patterns.iter() {
            match pattern.event_count {
                Some(0) | None => debug!("** Pattern [{}] (empty): {:?}", pattern_name(pattern.index), pattern),
                Some(_) => info!("** Pattern [{}]: {:?}", pattern_name(pattern.index), pattern)
            }
        }    
    }

    Ok(())
}

// Read generator controls data chunk(s)
pub fn read_controls_chunk(f: &mut dyn Read) -> std::io::Result<Vec<CtlData>> {
    let evt = read_chunk_event(f, SFChunkId::Id(SF_CONTROLS))?;
    Ok(read_controls(f, &evt)?)
}

pub fn read_controls(f: &mut dyn Read, evt: &SFChunk) -> std::io::Result<Vec<CtlData>> {
    let control_count = evt.data / mem::size_of::<CtlData>() as u32;
    info!("** Control count: {}", control_count);

    // We can get the raw data for controls, but there's no straight way to map them
    // without having (embedding here) all the specific control mapping for each of 
    // the internal generators/fx in Orion. This includes value ranges and if it's 
    // int or float value (CtlData or CtlDataFloat is used for same data struct.
    // Now we just dump the int and float versions and let user see which makes more sense.
    // Also we don't have the control name mappings. 

    let mut controls = Vec::with_capacity(control_count as usize);

    union ControlValue {
        i: i32,
        f: f32
    };
    impl std::fmt::Debug for ControlValue {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("ControlValue")
             .field("i", unsafe { &self.i })
             .field("f", unsafe { &self.f })
             .finish()
        }
    }
    for control_index in 0..control_count {
        let control = read_struct_unsafe::<CtlData>(f)?;
        let control_value = ControlValue { i: control.iCtrlValue };
        debug!("**** Control #{}: [{:?}]", control_index, control_value);
        //trace!("**** CtlData #{}: {:?}", control_index, control);
        controls.push(control);
    }
    //skip_chunk(f, evt.data)?;

    Ok(controls)
}

pub fn read_fx_assigns_header(f: &mut dyn Read, evt: &SFChunk, _file_version: u32) -> std::io::Result<()> {
    let fx_assigns_header = read_sized_struct_unsafe::<FxAssignHeader>(f, evt.data)?;

    info!("FX Assignments: [Send count: {}], [Master inserts (Orion Pro): {}]", fx_assigns_header.bSends, fx_assigns_header.bMaster);

    for assign_index in 0..(::std::cmp::min(fx_assigns_header.bSends as u32, FX_SENDS) as usize) {
        info!("** Send #{}: [{}]", assign_index + 1, module_index_to_str(fx_assigns_header.iSendID[assign_index] as i32));
    }

    // Only in Orion Pro saved files
    for assign_index in 0..(::std::cmp::min(fx_assigns_header.bMaster as u32, FX_MAIN_INSERTS) as usize) {
        info!("** Master Insert #{}: [{}]", assign_index + 1, module_index_to_str(fx_assigns_header.iMasterID[assign_index] as i32));
    }

    Ok(())
}

pub fn read_fx_returns_header(f: &mut dyn Read, _evt: &SFChunk, file_version: u32) -> std::io::Result<()> {
    info!("Master Channels:");
    if file_version >= 0x8000 {
        for channel_index in 0..(FX_MASTERCHANS - 1) {
            read_channel_info(f, channel_index, file_version)?;
        }
        // MASTER fixed to last channel
        read_channel_info(f, FX_MASTERCHANS-1 /* MASTER */, file_version)?;
    } else if file_version > 0x3100 {
        for channel_index in 0..(FX_MASTERCHANS_PRE_8000 - 1) {
            read_channel_info(f, channel_index, file_version)?;
        }
        // MASTER fixed to last channel
        read_channel_info(f, FX_MASTERCHANS-1 /* MASTER */, file_version)?;
    } else {
        // OLDER VERSIONS
        for channel_index in 0..FX_MASTERCHANS_PRE_3100 {
            // 4 SENDS
            read_channel_info(f, channel_index, file_version)?;
        }
        // No busses

        if file_version > 0x00002500 {
            // MASTER fixed to last channel
            read_channel_info(f, FX_MASTERCHANS-1 /* MASTER */, file_version)?;
        }
    }

    Ok(())
}     

pub fn read_channel(f: &mut dyn Read, evt: &SFChunk, file_version: u32) -> std::io::Result<()> {
    // Normally Orion creates channels when initializing generators (mono or generator >1channels) and trusts 
    // that number to match, but luckily it is also stored in the chunk evt.data as number. This loading style in Orion 
    // might be reason for some song load crashes in case of "SF_CHAN_INFO count != created channel count".

    // TODO: add code to count generator channels and warn mismatch
    let channel_count = evt.data;

    for channel_index in 0..channel_count {
        read_channel_info(f, channel_index, file_version)?;
    }
    Ok(())
}

pub fn read_channel_info(f: &mut dyn Read, channel_index: u32, _file_version: u32) -> std::io::Result<()> {
    let channel_info_evt = read_chunk_event(f, SFChunkId::Id(SF_CHAN_INFO))?;
    if !channel_info_evt.id == SF_CHAN_INFO {
        return Err(Error::new(ErrorKind::Other, format!("Channel data corrupt [{}].", channel_index)))
    }
    let channel_info = read_sized_struct_unsafe::<ChanInfo>(f, channel_info_evt.data)?;

    info!("Channel #{}: [{}] [Stereo: {}]", channel_index, fixed_c_string(&channel_info.achName), bool_to_str(channel_info.bIsStereoChan > 0));
    info!("** Insert #1: [{}]", module_index_to_str(channel_info.iInsert1));
    info!("** Insert #2: [{}]", module_index_to_str(channel_info.iInsert2));
    info!("** Insert #3: [{}]", module_index_to_str(channel_info.iInsert3));
    info!("** Insert #4: [{}]", module_index_to_str(channel_info.iInsert4));

    if bit_flag(channel_info.dwFlags, CHANFLAGS_EQS_EXTENDED) {
        info!("** EQS_LO12: [{}]", bool_to_str(bit_flag(channel_info.dwFlags, CHANFLAGS_EQS_LO12)));
        info!("** EQS_HI12: [{}]", bool_to_str(bit_flag(channel_info.dwFlags, CHANFLAGS_EQS_HI12)));
        info!("** EQS_CLIPPED: [{}]", bool_to_str(bit_flag(channel_info.dwFlags, CHANFLAGS_EQS_CLIPPED)));
        info!("** SEND_LINK: [{}]", bool_to_str(bit_flag(channel_info.dwFlags, CHANFLAGS_SEND_LINK)));
        info!("** EQS_POSTFX: [{}]", bool_to_str(bit_flag(channel_info.dwFlags, CHANFLAGS_EQS_POSTFX)));
    }

    // ------ Channel Data
    read_controls_chunk(f)?;
    // TODO: compatibility for read channel control values; now data is not displayed so no need
    /*
    // adjust pan, eq's etc. for older file versions
    if file_version < 0x1200
    {
        control_data[EVENT_CH_PAN].iCtrlValue -= 16384;
    }
    if file_version < 0x1300
    {
        control_data[EVENT_CH_EQ_LOSHELF].iCtrlValue -= 16384;
        control_data[EVENT_CH_EQ_HISHELF].iCtrlValue -= 16384;
        control_data[EVENT_CH_EQ_GAIN1].iCtrlValue -= 16384;
        control_data[EVENT_CH_EQ_GAIN2].iCtrlValue -= 16384;
    }
    if file_version < 0x2100
    {
        for(int i = 0; i < MAX_CHAN_CTRLS; i++)
        {
            control_data[i].iCookedValue = 0;
            if control_data[i].iMidiCtrl > 255 {
                control_data[i].iMidiCtrl = -1;
            }
        }
        control_data[EVENT_CH_EQ_GAIN1].iCtrlValue /= 2;
        control_data[EVENT_CH_EQ_GAIN2].iCtrlValue /= 2;
        control_data[EVENT_CH_EQ_LOSHELF].iCtrlValue /= 2;
        control_data[EVENT_CH_EQ_HISHELF].iCtrlValue /= 2;
    }
    if file_version < 0x2504
    {
        m_pInsert[2] = NULL;
        m_pInsert[3] = NULL;
    }
    if file_version < 0x2800
    {
        control_data[EVENT_CH_EQ_Q1].iCtrlValue += 16384;
        if(control_data[EVENT_CH_EQ_Q1].iCtrlValue > 32768)
            control_data[EVENT_CH_EQ_Q1].iCtrlValue = 32768;
    }
    if file_version < 0x3200
    {
        control_data[EVENT_CH_BUS1].iCtrlValue = 1;
        control_data[EVENT_CH_BUS2].iCtrlValue = 0;
        control_data[EVENT_CH_BUS3].iCtrlValue = 0;
        control_data[EVENT_CH_BUS4].iCtrlValue = 0;
        control_data[EVENT_CH_BUS5].iCtrlValue = 0;
    }
    */

    Ok(())
}

pub fn read_audio_snippets(f: &mut dyn Read, evt: &SFChunk, _file_version: u32) -> std::io::Result<()> {
    let audio_snippets_count = evt.data;
    info!("Audio snippet count: [{}]", audio_snippets_count);

    for audio_snippet_index in 0..audio_snippets_count {
        let sample_info2 = read_struct_unsafe::<SampleInfo2>(f)?;

        let sample_filename_length = read_u32(f)?;
        let sample_filename = read_utf8_string(f, sample_filename_length)?;

        let sample_sample_count = sample_info2.m_dwEnd;

        info!("** Audio #{} filename: [{}]", audio_snippet_index, sample_filename);
        info!("**** Sample count: [{}]", sample_sample_count);
        debug!("** SampleInfo2: {:?}", sample_info2);

        /*
        // load audio data if bundled, however seems this feature is disabled in Orion (too large files?)
        // and it's fixed to 16-bit audio
        if song.bundled_audio {
            skip data -> sample_sample_count*2
        }
        */
    }

    Ok(())
}

pub fn read_gen_midi_machine(f: &mut dyn Read, file_version: u32) -> std::io::Result<()> {
    let midi_device_index = read_u32(f)?;
    // Noticed that there's buffer leak in Orion, byte value is written as 4 bytes and read back as such
    // the extra bytes seem to be part of the control map data (mapping CC's for controls). But as it's
    // Internal data it could be something else too. So it's a bug in Midi Machine load/save code.
    let midi_channel = read_u8(f)?; /* skip invalid bytes >> */ read_u8(f)?; read_u8(f)?; read_u8(f)?;
    let midi_patch = read_u32(f)?;
    let midi_bank = read_u32(f)?;

    let midi_device_name;
    if file_version > 0x4100 {
        let midi_device_name_len = read_u32(f)?;
        midi_device_name = read_utf8_string(f, midi_device_name_len)?;    
    } else {
        // Not available in older versions
        midi_device_name = "".to_string();
    }

    info!("** Midi Device: [{}] [{}]", midi_device_index, midi_device_name);
    info!("** Midi channel: [{}]", midi_channel);
    info!("** Midi patch/bank: [{}/{}]", midi_patch, midi_bank);

    Ok(())
}

pub fn read_gen_audiotrack(f: &mut dyn Read, header: &GenHeader, file_version: u32) -> std::io::Result<()> {

    let generator_version = header.bGenVersion as u8;

    let event_size = if file_version >= 0x6160 {
        mem::size_of::<AudioEvent>()
    } else if file_version >= 0x2900 {
        mem::size_of::<OldAudioEvent>()
    } else {
        mem::size_of::<VeryOldAudioEvent>()
    } as u32;
    
    let audiotrack_attenuation = match generator_version {
        0 => VERYOLDAUDIOTRACK_ATTENUATION,
        1 => OLDAUDIOTRACK_ATTENUATION,
        2 | _ => AUDIOTRACK_ATTENUATION
    };
    info!("** Attenuation factor: [{}]", audiotrack_attenuation);

    let evt = read_chunk_event(f, SFChunkId::Id(SF_EVTS))?;
    skip_chunk(f, evt.data)?; // NOP

    info!("** Audio event/slices count: [{}]", evt.data / event_size);
    // TODO: event conversions for different file versions
    /*
    if file_version < 0x6160 {
        // older versions need def params!
        pEvt->dAttack[1] = 1;
        pEvt->dRelease[0] = 1;
        pEvt->dRelease[1] = 1;        
    }
    if file_version < 0x4300 {
        // DWORD to double conversion
        pEvt->dTime = pEvt->dwTimeOLD;
        pEvt->dLength = pEvt->dwLengthOLD;
    }
     // old files might have invalid slice events (those dreaded 1 tick slices)
    if file_version <= 0x7608 {
        if(pEvt->dLength <= 1.5) {
            delete pEvt
        }
    }
    */

    Ok(())
}

pub fn read_gen_rewire(f: &mut dyn Read, _gen_header: &GenHeader, _file_version: u32) -> std::io::Result<()> {
    let rewire_name_size = read_u32(f)?;
    let rewire_name = read_utf8_string(f, rewire_name_size)?;
    info!("** ReWire device name: [{}]", rewire_name);

    Ok(())
}

pub fn read_gen_ultran(f: &mut dyn Read, _gen_header: &GenHeader, _file_version: u32) -> std::io::Result<()> {
    let program_evt = read_subchunk_event(f, SFChunkId::Id(SF_PROGRAM))?;
    let program_filename = read_utf8_string(f, program_evt.data)?; // max 255 bytes in Orion, here any size supported

    let gen_data_evt = read_subchunk_event(f, SFChunkId::Id(SF_GEN_DATA))?;
    let ultran_header = read_sized_chunk_data::<UltranSaveHdr>(f, gen_data_evt)?;

    info!("** Program filename: [{}]", program_filename);
    info!("** Program type: [{}]", program_type_to_str(ultran_header.iPrgType as u32));
    info!("** Selected program: [{}]", ultran_header.iSelProg);
    info!("** Selected program: [{}]", ultran_header.iSelProg);
    info!("** Program bundled size: [{} bytes]", ultran_header.dwBundleSize);
    debug!("** Wave data: [{:?}]", ultran_header.wavedat);

    Ok(())
}

pub fn read_gen_xr909(f: &mut dyn Read, _gen_header: &GenHeader, file_version: u32) -> std::io::Result<()> {

    if file_version > 0x0905 {
        // load user samples
        let sample_pool_evt = read_subchunk_event(f, SFChunkId::Id(SF_SMP_POOL))?;
        let sample_pool_size = sample_pool_evt.data;

        info!("** User sample count: [{}]", sample_pool_size);
        for _sample_index in 0..sample_pool_size {
            // read sample header
            let sample_evt = read_subchunk_event(f, SFChunkId::Id(SF_SMP_HEADER))?;
            let sample_header = read_sized_chunk_data::<SmpHeader>(f, sample_evt)?;

            let sample_data_evt = read_subchunk_event(f, SFChunkId::Id(SF_SMP_DATA))?;
            let sample_data_size = sample_data_evt.data;
            skip_chunk(f, sample_data_size)?;

            // always (?) sample_header.dwFormat == 0 => 16-bit, mono
            info!("**** {}: [format: {}, size: {} bytes]", 
                xr909_channel_to_str(sample_header.dwSmpNum + XR909_SMP_USER_1), 
                sample_header.dwFormat,
                sample_data_size
            );
        }
    }

    let sound_version = 0;

    // NEW since 2.7: version info for all 909 sounds
    if file_version > 0x2700 {
        let info_evt = read_subchunk_event(f, SFChunkId::Id(SF_GEN_INFO))?;
        let version_info_count = info_evt.data;

        // Version is read as byte array in Orion, we read byte by byte
        // It is limited by size of XR909_CHANS, but read what's in file which might be larger.
        for version_info_index in 0..version_info_count {
            let version = read_u8(f)?;
            info!("**** [{}] version: [{}]", xr909_channel_to_str(version_info_index), version);
        }
    } else {
        // TODO: all versions are 0
        info!("**** Sound version: [{}]", sound_version);        
    }

    Ok(())
}

pub fn read_gen_pro9(f: &mut dyn Read, _gen_header: &GenHeader, file_version: u32) -> std::io::Result<()> {

    if file_version > 0x7926 {
        let gen_info_evt = read_subchunk_event(f, SFChunkId::Id(SF_GEN_INFO))?;
        if gen_info_evt.data == 4 {
            let current_rythm_id = read_i32(f)?;
            info!("** Current rythm Id: [{}]", current_rythm_id);
        } else {
            // for safety skip it if it's not 4 byte size
            skip_chunk(f, gen_info_evt.data)?;
        }
    }

    Ok(())
}

pub fn read_gen_automationtrack(f: &mut dyn Read, _gen_header: &GenHeader, _file_version: u32) -> std::io::Result<()> {
    // Supported, but are these used really anywhere?
    let device_id = read_u32(f)?;
    let channel = read_u32(f)?;
    let patch = read_u32(f)?;
    let bank = read_u32(f)?;
    let is_automation_track = read_u32(f)?;
    let auto_module = read_u32(f)?;
    let auto_channel = read_u32(f)?;
    let auto_control_id = read_u32(f)?;

    info!("** Device ID: [{}]", device_id);
    info!("** Channel: [{}]", channel);
    info!("** Patch/Bank: [{}/{}]", patch, bank);
    info!("** Is Automation Track: [{}]", is_automation_track);
    info!("** Auto module: [{}]", auto_module);
    info!("** Auto channel: [{}]", auto_channel);
    info!("** Auto control ID: [{}]", auto_control_id);

    Ok(())
}


pub fn read_gen_drumrack(f: &mut dyn Read, _gen_header: &GenHeader, file_version: u32) -> std::io::Result<()> {

    let evt = read_subchunk_event(f, SFChunkId::Id(SF_TRACK_DATA))?;
    let track_count = evt.data;

    let control_count = track_count * DM_TRACK_CTRLS;
    info!("** Control count (all tracks): [{}]", control_count);

    let non_rec_data_size = if file_version >= 0x4200 {
        mem::size_of::<NonRecData>() as u32
    } else {
        12u32 // magic size of old NonRecData struct (fills only first 12 bytes of the struct new struct)
    };

    // TODO: temporary holder of data for now, we are not collecting these besides displaying info!
    // The SampleInfo contains reference to packed PCMWAVEFORMAT. Access to that is tricky.
    #[derive(Debug, Default)] 
    struct DrumTrack {
        index: u32,
        version: u8,
//        length: u32,
//        event_count: Option<u32>,
        name: String,
        sub_sample_count: i16,
        sample_infos: Vec<SampleInfo>,
        samples: Vec<Vec<u8>> // just chunk of bytes now
    };
    let mut tracks: Vec<DrumTrack> = Vec::with_capacity(track_count as usize);

    for track_index in 0..track_count {
        debug!("read_gen_drumrack:track_index={}", track_index);

        // Track header data
        let track_data = read_sized_struct_unsafe::<NonRecData>(f, non_rec_data_size)?;
        // Next load the track specific data depending on version(s)

        let mut track = DrumTrack::default();

        track.index = track_index;
        track.version = track_data.bTrackVersion;
        track.sub_sample_count = track_data.nSubSamples;
        track.samples = Vec::new();

        if track_data.bTrackVersion < 3 {
            // old versions < 3 of track data
            if track_data.nLen > 0 {
                track.name = read_utf8_string(f, track_data.nLen)?;

                if u8_as_bool(track_data.bAsBundle) {
                    // Only read 40 first bytes, rest is considered empty
                    let mut sample_info = read_sized_struct_unsafe::<SampleInfo>(f, 40)?;
                    let sample_sample_count = sample_info.m_dwEnd; // number of *samples*
                    // Old track only supports 16-bit (2 bytes per sample)
                    track.samples.push(skip_chunk(f, sample_sample_count * 2)?);

                    // Defaults not in structs
                    // TODO: not all set here
                    sample_info.m_bHighVel = 128;
                    track.sub_sample_count = 1;

                    track.sample_infos.push(sample_info);
                } else {
                    // no samples bundled
                    // Orion would now load sample specified in the track.name and populate sample_info
                }
            }
        } else {
            // version >= 3 of track data
            track.sub_sample_count = track_data.nSubSamples;
            if track.sub_sample_count > 0 {
                track.name = read_utf8_string(f, track_data.nLen)?;

                if track_data.bTrackVersion == 3 {
                    // version == 3
                    for _ in 0..track.sub_sample_count {
                        // Old SampleInfo conversion to new SampleInfo
                        let sample_info_v3 = read_struct_unsafe::<SampleInfoDrumsOld>(f)?;
                        let sample_info = SampleInfo {
                            m_wavHeader: sample_info_v3.m_wavHeader,
                            m_dwStart: sample_info_v3.m_dwStart,
                            m_dwEnd: sample_info_v3.m_dwEnd,
                            m_dwLoopStart: sample_info_v3.m_dwLoopStart,
                            m_dwLoopEnd: sample_info_v3.m_dwLoopEnd,
                            m_bLoopSwitch: sample_info_v3.m_bLoopSwitch,
                            m_bRootKey: sample_info_v3.m_bRootKey,
                            m_bDontDelete: sample_info_v3.m_bDontDelete,
                            m_bModified: sample_info_v3.m_bModified,
                            m_achPath: sample_info_v3.m_achPath,
                            m_bLowVel: sample_info_v3.m_bLowVel,
                            m_bHighVel: sample_info_v3.m_bHighVel,
                            // This for having other fields as zero (no ::default in SampleInfo)
                            ..unsafe { std::mem::zeroed::<SampleInfo>() }
                        };
                        track.sample_infos.push(sample_info);
                    }
                } else { // version > 3
                    for _ in 0..track.sub_sample_count {
                        let sample_info = read_struct_unsafe::<SampleInfo>(f)?;
                        track.sample_infos.push(sample_info);
                    }
                }

                if u8_as_bool(track_data.bAsBundle) {
                    // TODO: branching could be a bit more elegant, now mirroring Orion source code
                    if track.sample_infos.len() > 0 {
                        let bits = track.sample_infos[0].m_wavHeader.wBitsPerSample;
                        match bits {
                            32 => {
                                for sub_sample_index in 0..(track.sub_sample_count as usize) {
                                    let sample_sample_count = track.sample_infos[sub_sample_index].m_dwEnd;
                                    // 4 bytes per sample
                                    track.samples.push(skip_chunk(f, sample_sample_count * 4)?);
                                }
                            },
                            16 => {
                                for sub_sample_index in 0..(track.sub_sample_count as usize) {
                                    let sample_sample_count = track.sample_infos[sub_sample_index].m_dwEnd;
                                    // 2 bytes per sample
                                    track.samples.push(skip_chunk(f, sample_sample_count * 2)?);
                                }
                            },
                            _ => {} /* NOP, no other sample format is supported */
                        }
                    }
                } else {
                    // no samples bundled
                    // Orion would now load sample specified in the sample_info(s) and populate those
                }
            }
        }

        info!("** Track #{}: [{}]", track.index, track.name);
        info!("**** Sub sample count: [{}]", track.sub_sample_count);
        
        for sub_sample_index in 0..(track.sub_sample_count as usize) {
            let si = track.sample_infos[sub_sample_index];
            // Check if we got sample data for size value
            let sample_size = if sub_sample_index < track.samples.len() {
                track.samples[sub_sample_index].len()
            } else {
                0
            };
            let bits = si.m_wavHeader.wBitsPerSample;
            info!("**** Sub sample #{}: [{}] [{} bit, {} bytes]", 
                sub_sample_index + 1,
                fixed_c_string(&si.m_achPath),
                bits,
                sample_size
            );
        }

        tracks.push(track);
    }

    Ok(())
}

pub fn read_gen_wavefusion(f: &mut dyn Read, _gen_header: &GenHeader, file_version: u32) -> std::io::Result<()> {
	if file_version > 0x0904 {
        let wt_info_id = read_u32(f)?;
        if wt_info_id == SF_WT_INFO {
            let wt_filename_size = read_u32(f)?;
            let wt_filename = read_utf8_string(f, wt_filename_size)?;
            info!("** Wavetable filename: [{}]", wt_filename);
        }
	}

	if file_version < 0x3600 {
        // old version!
        // TODO: some controls need defaults
        /*
		for(int i=0; i <= 8; i+=4)
		{
			m_pCtlData[EVENTW_OSC1MUTE+i].iCtrlValue = 0;
			m_pCtlData[EVENTW_OSC1LFO1+i].iCtrlValue = 1;
			m_pCtlData[EVENTW_OSC1LFO2+i].iCtrlValue = 1;
			m_pCtlData[EVENTW_OSC1MOD+i].iCtrlValue = 1;
		}
        m_pCtlData[EVENTW_MODENV_DEST].iCtrlValue = 0;
        */
    }
    Ok(())
}

pub fn read_gen_wasp(_f: &mut dyn Read, gen_header: &GenHeader, _file_version: u32) -> std::io::Result<()> {
    if gen_header.bGenVersion < 3 {
        /*
        m_iNumOuts = 1;
        m_nRenderMode = RENDER_MONO_CHANS;
        m_bUsesStereoChan = FALSE;
        m_pCtlData[ID_WASP_VEL_AMP].iCtrlValue = 128;
        m_pCtlData[ID_WASP_VEL_FILTER].iCtrlValue = 0;
        */
    } else if gen_header.bGenVersion == 3 {
        /*
        // convert velocity amp/filter switch to knob
        if(m_pCtlData[ID_WASP_VEL_AMP].iCtrlValue == 0)
        {
            m_pCtlData[ID_WASP_VEL_AMP].iCtrlValue = 128;
            m_pCtlData[ID_WASP_VEL_FILTER].iCtrlValue = 0;
        }
        else
        {
            m_pCtlData[ID_WASP_VEL_FILTER].iCtrlValue = 128;
            m_pCtlData[ID_WASP_VEL_AMP].iCtrlValue = 0;
        }
        
        // convert kbtrk switch to knob
        if(m_pCtlData[ID_WASP_FLT_KBTRACK].iCtrlValue > 0)
            m_pCtlData[ID_WASP_FLT_KBTRACK].iCtrlValue = 128;

        m_pCtlData[ID_WASP_WHITE_NOISE].iCtrlValue = 0;
        m_pCtlData[ID_WASP_VOLUME].iCtrlValue = 64;
    
        m_bGenVersion = 4;
        */
    }
    Ok(())
}

pub fn read_native_generator(f: &mut dyn Read, evt: &SFChunk, current_module_id: i32, file_version: u32) -> std::io::Result<GenHeader>  {
    let gen_header = read_gen_native_header_chunk(f, evt.id, current_module_id)?;
    read_generator_tracks(f, gen_header.dwFlags)?;
    read_patterns(f, file_version)?;
    read_controls_chunk(f)?;
    Ok(gen_header)
}

pub fn read_gen_sampler(f: &mut dyn Read, _gen_header: &GenHeader, file_version: u32) -> std::io::Result<()> {
    // Sampler specific data loading
    if file_version > 0x0908 {
        let evt = read_subchunk_event(f, SFChunkId::Id(SF_PROGRAM))?;

        let mut str_len: u32 = evt.data;
        // For some reason there's this special handling..
        if file_version > 0x0910 && file_version < 0x1370 {
			str_len -= 4;
        }
        let data = read_dynamic_chunk_data(f, str_len)?;
        // Assuming utf-8 encoding for simplicity as we have non nul terminated string
        let program_filename = match String::from_utf8(data) {
            Ok(filename) => filename,
            Err(_) => "".to_string()
        };
        info!("** Sampler program name: [{}]", program_filename);

        if file_version >= 0x1370 {
            // More extended data follows
            let evt = read_subchunk_event(f, SFChunkId::Id(SF_GEN_DATA))?;
            let data = read_sized_chunk_data::<SmpFileHdr>(f, evt)?;

            info!("** Sampler program: [type: {}] [selected: {}] [override FX: {}]", 
                data.iPrgType, 
                data.iSelProg, 
                bool_to_str(bit_flag(data.dwFlags, SAMPLER_FLAGS_FX_OVERRIDE)));
            info!("** Sampler bundled data: [{} bytes]", data.dwBundleSize);
            if bit_flag(data.dwFlags, SAMPLER_FLAGS_BUNDLE) {
                read_dynamic_chunk_data(f, data.dwBundleSize)?; // NOP
            }
        } else if file_version > 0x0910 && file_version < 0x1370 {
            // Very old files just have mostly defaults (as set in Orion)
            let sel_program = read_u32(f)?;
            info!("** Sampler program: [type: {}] [selected: {}] [override FX: {}]", 
                PROGRAMTYPE_ORION, 
                sel_program, 
                bool_to_str(false));
        }

    }

    Ok(())
}

pub fn load_song_data(filename: &str, song: &mut OrionSong) -> std::io::Result<()> {

    song.filename = PathBuf::from(filename).canonicalize()?.into_os_string().into_string().unwrap();

    let mut file = fs::File::open(filename)?;
    let f = Read::by_ref(&mut file);

    // Header checks, maybe move to the while loop?
    let evt = read_chunk_event(f, SFChunkId::Any())?;
    // SF_HEADER data size is always 4 bytes (file version)
    if evt.id == SF_HEADER && evt.data == 4 {
        song.version = read_u32(f)?;
        info!("File version {:x}", song.version);
    } else {
        error!("Not a song or header is corrupt {:?}", evt);
        return Err(Error::new(ErrorKind::Other, "Not a song or header is corrupt"))
    }

    // Read chunks in sequence
    // simple error handling, probably chockes in broken songs now
    let mut done = false;
    //let mut broken = false;
    let mut current_module_id = -1;
    while !done /*&& !broken*/ {
        let evt = read_chunk_event(f, SFChunkId::Any())?;

        if evt.id == 0 && evt.data == 0 {
            error!("Broken chunk={:?}", evt);
            return Err(Error::new(ErrorKind::Other, "Song is corrupted"));
        }

        match evt.id {
            SF_HEADER => {
                skip_chunk(f, evt.data)?; // NOP
            },
            SF_GLOB => {
                // if file_version < 0x1360; GLOB IS DIFFERENT TODO 
                // else GlobCfg
                skip_chunk(f, evt.data)?; // NOP

                // GLOB has sub-chunk with same id as SF_OBJ_VST == EVENT_SONGINFO
                read_subchunk_event(f, SFChunkId::Id(EVENT_SONGINFO))?;
                let song_info = read_chunk_data::<SONGINFO>(f)?;
                debug!("** SONGINFO {:?}", song_info);

                // Let's just skip loading the song info strings, only stored if length > 1 (including NUL),
                // but stored with NUL.
                if song_info.dwLenGeneral > 1 {
                    let song_description = read_utf8_string(f, song_info.dwLenGeneral)?;
                    info!("** Description:\n{}", song_description);
                }
                if song_info.dwLenURL > 1 {
                    let song_url = read_utf8_string(f, song_info.dwLenURL)?;
                    info!("** URL: [{}]", song_url);    
                }
            },
            SF_EVTS => {
                // global events
                if evt.data > 0 {
                    if song.version < 0x2300 {
                        // older versions had 12-byte events (should convert?)
                        skip_chunk(f, evt.data)?; // NOP
                    } else {
                        skip_chunk(f, evt.data)?; // NOP
                    }            
                }
            },
            SF_PATC => {
                // pattern data
                skip_chunk(f, evt.data)?; // NOP
            },
            SF_OBJS => {
                let module_count = read_u32(f)?;
                info!("Module count (stored): {}", module_count);
            },
            SF_SCENES => {
                // Scenes was a feature never made public; so no songs should really have this
                skip_chunk(f, evt.data)?; // NOP
            },
            SF_EOF => {
                done = true;
                debug!("EOF reached {:?}, all done!", evt);
            },
            SF_WINP => {
                let window_count = read_u32(f)?;
                read_window_positions(f, window_count)?;
            },
            SF_TRACK_DATA => {
                error!("Unmatched track data encountered {:?}", evt);
                skip_chunk(f, evt.data)?; // NOP
            },
            // MODULES
            SF_GEN_INFO => {
                skip_chunk(f, evt.data)?; // NOP, unprocessed chunk found
            },
            SF_MODULE_END => {
                skip_chunk(f, evt.data)?; // NOP
            },
            SF_OBJ_SAMPLER => {
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;

                // SF_OBJ_SAMPLER specific
                read_gen_sampler(f, &gen_header, song.version)?;

            },
            SF_OBJ_WASP => {
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;

                // SF_OBJ_WASP specific
                read_gen_wasp(f, &gen_header, song.version)?;
            },
            SF_OBJ_VST2SYNTH => { // VST2 synth
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                let gen_header = read_gen_vst2synth_header(f, evt.id, current_module_id, song.version)?;
                read_generator_tracks(f, gen_header.dwFlags)?;
                read_patterns(f, song.version)?;

                // SF_OBJ_VST2SYNTH specific
                read_gen_vst2synth(f, &gen_header, current_module_id, song.version)?;
            },
            SF_OBJ_VST => { // VST2 effect
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                let fx_header = read_gen_vst2effect_header(f, evt.id, current_module_id, song.version)?;

                // SF_OBJ_VST specific
                read_gen_vst2effect(f, &fx_header, song.version)?;
            },
            SF_OBJ_MIDI => { // Midiout
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let _gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;

                read_gen_midi_machine(f, song.version)?;
            },
            SF_OBJ_AUDIO => { // Audiotrack
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;

                read_gen_audiotrack(f, &gen_header, song.version)?;
            },
            SF_OBJ_DRUMS => {
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;

                read_gen_drumrack(f, &gen_header, song.version)?;
            },
            SF_OBJ_DXSYNTH => {
                // TODO!!!!
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                read_dx_synth_header(f, evt.id, current_module_id, song.version)?;
            },
            SF_OBJ_DX => {
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                read_dx_effect_header(f, evt.id, current_module_id, song.version)?;

            },
            SF_OBJ_WAVE => { // WaveFusion
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)
                
                // generic generator reader
                let gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;

                read_gen_wavefusion(f, &gen_header, song.version)?;
            },
            SF_OBJ_909 => { // XR-909
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;

                read_gen_xr909(f, &gen_header, song.version)?;
            },
            SF_OBJ_P9 => { // Pro-9
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;

                read_gen_pro9(f, &gen_header, song.version)?;
            },
            SF_OBJ_303 => { // Monobass
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let _gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;
                // outputs = 1, mono
            },
            SF_OBJ_TOMKICK => { // Tomcat
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let _gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;
                // outputs = 1, mono
            },
            SF_OBJ_HIHATS => { // Reserved
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let _gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;
            },
            SF_OBJ_SCREAMER => { // Screamer
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let _gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;
                // outputs = 2, stereo
            },
            SF_OBJ_PLUCKED => { // Plucked String
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let _gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;
            },
            SF_OBJ_ULTRAN => { // Ultran
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;

                read_gen_ultran(f, &gen_header, song.version)?;
            },
            SF_OBJ_REWIRE => { // ReWire track
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                // on rewire probably not neccessary to display this info?
                let gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;

                read_gen_rewire(f, &gen_header, song.version)?;
            },
            SF_OBJ_FMSYNTH => { // Reserved
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let _gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;
            },
            SF_OBJ_GRAND => { // Grand Piano (Orion >=8)
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let _gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;
            },
            SF_OBJ_FIRSTFX ..= SF_OBJ_LASTFX => { // All internal effects
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                let fx_header = read_native_effect_header(f, evt.id, current_module_id, song.version)?;

                // Specific FX readers if it's not basic
                match evt.id {
                    SF_OBJ_PCF => {
                        read_native_effect_pcf(f, &fx_header, song.version)?;
                    },
                    SF_OBJ_FFT_3 => {
                        read_native_effect_fft_3(f, &fx_header, song.version)?;
                    },
                    _ => { /* NOP */ }
                };
            },
            SF_OBJ_AUTOMATION => {
                // Support loading these, but it's probably deprecated feature in Orion, seems to
                // be sort of Midi generator (simple Midi Out).
                current_module_id += 1;
                skip_chunk(f, evt.data)?; // NOP (data==0)

                // generic generator reader
                let gen_header = read_native_generator(f, &evt, current_module_id, song.version)?;

                read_gen_automationtrack(f, &gen_header, song.version)?;
            },
            SF_FX_ASSIGNS => { // Mixer/Master Send and Insert assigments
                read_fx_assigns_header(f, &evt, song.version)?;
            },
            SF_FX_RETURNS => {
                read_fx_returns_header(f, &evt, song.version)?;
            },
            SF_CHAN => {
                read_channel(f, &evt, song.version)?;
            },
            SF_AUDIO => {
                // References to audio clips
                read_audio_snippets(f, &evt, song.version)?;
            },
            SF_USERBITMAP => {
                let _bitmap = skip_chunk(f, evt.data)?;
                info!("User Info Bitmap (BMP): [{} bytes]", evt.data);
            },
            2007 => {
                // TODO: placeholder effect [repl]; not entirely sure about use of this
                skip_chunk(f, evt.data)?;
            }
            _ => {
                /* NOP */ 
                error!("Unhandled chunk encountered [{}] {:?}", chunk_id_to_str(evt.id), evt);
                skip_chunk(f, evt.data)?;
            }
        }

        // skip data for now to next chunk
        
        //fr.seek(std::io::SeekFrom::Current(evt.data as i64))?;
    }

    Ok(())
}
