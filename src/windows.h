// For some Windows header compatibility
//typedef unsigned int UINT, *UINT, *UINT;
typedef int LONG, *PLONG, *LPLONG;
typedef unsigned short WORD, *PWORD, *LPWORD;
typedef unsigned int DWORD, *PDWORD, *LPDWORD;
typedef unsigned char BYTE, *PBYTE, *LPBYTE;
typedef int			  BOOL;
typedef unsigned char BYTE;

#pragma pack(push, 1)

typedef struct _RECT {
   LONG left;
   LONG top;
   LONG right;
   LONG bottom;
} RECT;

typedef DWORD COLORREF;
typedef DWORD* LPCOLORREF;

// Windows audio
typedef struct waveformat_tag {
  WORD  wFormatTag;
  WORD  nChannels;
  DWORD nSamplesPerSec;
  DWORD nAvgBytesPerSec;
  WORD  nBlockAlign;
} WAVEFORMAT;

typedef struct {
  WAVEFORMAT wf;
  WORD       wBitsPerSample;
} PCMWAVEFORMAT;
#pragma pack(pop)

// DirectX
typedef struct _GUID {
  unsigned int  Data1;
  unsigned short Data2;
  unsigned short Data3;
  unsigned char  Data4[8];
} GUID;

typedef GUID CLSID;

typedef struct _CTime { 
    unsigned char data[8];
} CTime;
